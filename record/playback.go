package record

import (
	//	"time"
	"github.com/gordonklaus/portaudio"
	"github.com/mkb218/gosndfile/sndfile"
)

// Playback plays sound file
func Playback(filename string) (err error) {

	var inf *sndfile.Info
	inf = new(sndfile.Info)

	// Detect sound format
	f, err := sndfile.Open(filename, sndfile.Read, inf)
	if err != nil {
		return
	}
	defer f.Close()

	// Allocate a buffer for the frames * mono/stereo
	//	sz := int64(inf.Channels) * inf.Frames
	buf := make([]int32, 8192)

	err = portaudio.Initialize()
	if err != nil {
		return
	}
	defer portaudio.Terminate()

	dvc, err := portaudio.OpenDefaultStream(
		0,
		int(inf.Channels),
		float64(inf.Samplerate),
		len(buf),
		&buf,
	)
	if err != nil {
		return
	}
	defer dvc.Close()

	err = dvc.Start()
	if err != nil {
		return
	}
	defer dvc.Stop()

	for {

		// Read frames into the buffer
		//		num, err := f.ReadFrames(buf)
		_, err := f.ReadItems(buf)
		if err != nil {
			break
		}

		err = dvc.Write()
		if err != nil {
			break
		}

		// EOF can mean expected count is not actual frames read
		//		if num != inf.Frames {
		//			break
		//		}
	}

	return
}
