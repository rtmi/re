// Package grpc provides a gRPC client for the relay service.
package grpc

import (
	"time"

	jujuratelimit "github.com/juju/ratelimit"
	stdopentracing "github.com/opentracing/opentracing-go"
	"github.com/patterns/re/relay"
	"github.com/patterns/re/relay/pb"
	"github.com/sony/gobreaker"
	"google.golang.org/grpc"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/opentracing"
	grpctransport "github.com/go-kit/kit/transport/grpc"
)

// New returns an RelayService backed by a gRPC client connection. It is the
// responsibility of the caller to dial, and later close, the connection.
func New(conn *grpc.ClientConn, tracer stdopentracing.Tracer, logger log.Logger) relay.Service {
	// We construct a single ratelimiter middleware, to limit the total outgoing
	// QPS from this client to all methods on the remote instance. We also
	// construct per-endpoint circuitbreaker middlewares to demonstrate how
	// that's done, although they could easily be combined into a single breaker
	// for the entire remote instance, too.

	limiter := ratelimit.NewTokenBucketLimiter(jujuratelimit.NewBucketWithRate(100, 100))

	var connectEndpoint endpoint.Endpoint
	{
		connectEndpoint = grpctransport.NewClient(
			conn,
			"Relay",
			"Connect",
			relay.EncodeGRPCConnectRequest,
			relay.DecodeGRPCConnectResponse,
			pb.ConnectReply{},
			grpctransport.ClientBefore(opentracing.ToGRPCRequest(tracer, logger)),
		).Endpoint()
		connectEndpoint = opentracing.TraceClient(tracer, "Connect")(connectEndpoint)
		connectEndpoint = limiter(connectEndpoint)
		connectEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Connect",
			Timeout: 30 * time.Second,
		}))(connectEndpoint)
	}

	var joinEndpoint endpoint.Endpoint
	{
		joinEndpoint = grpctransport.NewClient(
			conn,
			"Relay",
			"Join",
			relay.EncodeGRPCJoinRequest,
			relay.DecodeGRPCJoinResponse,
			pb.JoinReply{},
			grpctransport.ClientBefore(opentracing.ToGRPCRequest(tracer, logger)),
		).Endpoint()
		joinEndpoint = opentracing.TraceClient(tracer, "Join")(joinEndpoint)
		joinEndpoint = limiter(joinEndpoint)
		joinEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Join",
			Timeout: 30 * time.Second,
		}))(joinEndpoint)
	}

	var chatEndpoint endpoint.Endpoint
	{
		chatEndpoint = grpctransport.NewClient(
			conn,
			"Relay",
			"Chat",
			relay.EncodeGRPCChatRequest,
			relay.DecodeGRPCChatResponse,
			pb.ChatReply{},
			grpctransport.ClientBefore(opentracing.ToGRPCRequest(tracer, logger)),
		).Endpoint()
		chatEndpoint = opentracing.TraceClient(tracer, "Chat")(chatEndpoint)
		chatEndpoint = limiter(chatEndpoint)
		chatEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Chat",
			Timeout: 30 * time.Second,
		}))(chatEndpoint)
	}

	var namesEndpoint endpoint.Endpoint
	{
		namesEndpoint = grpctransport.NewClient(
			conn,
			"Relay",
			"Names",
			relay.EncodeGRPCNamesRequest,
			relay.DecodeGRPCNamesResponse,
			pb.NamesReply{},
			grpctransport.ClientBefore(opentracing.ToGRPCRequest(tracer, logger)),
		).Endpoint()
		namesEndpoint = opentracing.TraceClient(tracer, "Names")(namesEndpoint)
		namesEndpoint = limiter(namesEndpoint)
		namesEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Names",
			Timeout: 30 * time.Second,
		}))(namesEndpoint)
	}

	return relay.Endpoints{
		ConnectEndpoint: connectEndpoint,
		JoinEndpoint:    joinEndpoint,
		ChatEndpoint:    chatEndpoint,
		NamesEndpoint:   namesEndpoint,
	}
}
