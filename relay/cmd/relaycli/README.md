# Relaycli

Relaycli is a consumer implementation to control Relaysvc.

## Running / Development

* `relaycli -grpc.addr=localhost:8002 -method=connect "irc.freenode.net:6667"`
* `relaycli -grpc.addr=localhost:8002 -method=join "#bot-test"`

## Further Reading / Useful Links

* [irc](https://github.com/sorcix/irc)
* [maildir](https://github.com/luksen/maildir)
* [go-kit](https://github.com/go-kit/kit/examples)



