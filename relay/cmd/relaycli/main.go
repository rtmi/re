package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sourcegraph.com/sourcegraph/appdash"
	appdashot "sourcegraph.com/sourcegraph/appdash/opentracing"

	"github.com/go-kit/kit/log"
	"github.com/patterns/re/relay"
	grpcclient "github.com/patterns/re/relay/client/grpc"
)

func main() {
	// The relaycli presumes no service discovery system, and expects users to
	// provide the direct address of a relaysvc. This presumption is reflected in
	// the relaycli binary and the the client packages: the -transport.addr flags
	// and various client constructors both expect host:port strings.

	var (
		grpcAddr    = flag.String("grpc.addr", "", "gRPC (HTTP) address of relaysvc")
		appdashAddr = flag.String("appdash.addr", "", "Enable Appdash tracing via an Appdash server host:port")
		method      = flag.String("method", "connect", "connect, join")
	)
	flag.Parse()

	count := len(flag.Args())
	if count < 1 && count > 2 {
		fmt.Fprintf(os.Stderr, "usage: relaycli [flags] <s>\n")
		os.Exit(1)
	}

	var tracer stdopentracing.Tracer
	{
		if *appdashAddr != "" {
			tracer = appdashot.NewTracer(appdash.NewRemoteCollector(*appdashAddr))
		} else {
			tracer = stdopentracing.GlobalTracer() // no-op
		}
	}

	var (
		service relay.Service
		err     error
	)
	if *grpcAddr != "" {
		conn, err := grpc.Dial(*grpcAddr, grpc.WithInsecure(), grpc.WithTimeout(time.Second))
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v", err)
			os.Exit(1)
		}
		defer conn.Close()
		service = grpcclient.New(conn, tracer, log.NewNopLogger())
	} else {
		fmt.Fprintf(os.Stderr, "error: no remote address specified\n")
		os.Exit(1)
	}
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	switch *method {
	case "connect":
		s := flag.Args()[0]
		v, err := service.Connect(context.Background(), s)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "Connect to %s: %s\n", s, v)

	case "join":
		c := flag.Args()[0]
		v, err := service.Join(context.Background(), c)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "Join channel %s: %s\n", c, v)

	case "chat":
		c := flag.Args()[0]
		m := flag.Args()[1]
		err := service.Chat(context.Background(), c, m)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "Chat %s %s\n", c, m)

	case "names":
		c := flag.Args()[0]
		err := service.Names(context.Background(), c)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "Names %s \n", c)

	default:
		fmt.Fprintf(os.Stderr, "error: invalid method %q\n", method)
		os.Exit(1)
	}
}
