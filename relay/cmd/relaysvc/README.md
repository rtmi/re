# Relaysvc

Relaysvc connects to IRC and saves messages into a local maildir store.

## Running / Development

* `relaysvc -debug.addr=localhost:8000 -grpc.addr=localhost:8002 -irc.nick=patterns -irc.secret=password`

## Further Reading / Useful Links

* [irc](https://github.com/sorcix/irc)
* [maildir](https://github.com/luksen/maildir)
* [go-kit](https://github.com/go-kit/kit/examples)



