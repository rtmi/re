package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"net/http/pprof"
	"os"
	"os/signal"
	"syscall"

	stdopentracing "github.com/opentracing/opentracing-go"
	"github.com/patterns/re/relay"
	"github.com/patterns/re/relay/pb"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sourcegraph.com/sourcegraph/appdash"
	appdashot "sourcegraph.com/sourcegraph/appdash/opentracing"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/tracing/opentracing"
)

func main() {
	var (
		debugAddr   = flag.String("debug.addr", ":8080", "Debug and metrics listen address")
		grpcAddr    = flag.String("grpc.addr", ":8082", "gRPC (HTTP) listen address")
		appdashAddr = flag.String("appdash.addr", "", "Enable Appdash tracing via an Appdash server host:port")
		ircNick     = flag.String("irc.nick", "gogrpctest", "IRC nickname")
		ircSecret   = flag.String("irc.secret", "", "IRC password")
		ircHost     = flag.String("irc.addr", "irc.freenode.net:6667", "IRC host system")
	)
	flag.Parse()

	// Logging domain.
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stdout)
		logger = log.NewContext(logger).With("ts", log.DefaultTimestampUTC)
		logger = log.NewContext(logger).With("caller", log.DefaultCaller)
	}
	logger.Log("msg", "hello")
	defer logger.Log("msg", "goodbye")

	// Metrics domain.
	var cx, cxerr, joins, chats, names metrics.Counter
	{
		// Business level metrics.
		cx = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "relay",
			Name:      "connect_succeeded",
			Help:      "Total count of connects via the Connect method.",
		}, []string{})
		cxerr = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "relay",
			Name:      "connect_failed",
			Help:      "Total count of connect errors via the Connect method.",
		}, []string{})
		joins = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "relay",
			Name:      "channels_joined",
			Help:      "Total count of channels joined via the Join method.",
		}, []string{})
		chats = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "relay",
			Name:      "chats_sent",
			Help:      "Total count of chats sent via the Chat method.",
		}, []string{})
		names = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "relay",
			Name:      "names_sent",
			Help:      "Total count of calls sent via the Names method.",
		}, []string{})
	}
	var duration metrics.Histogram
	{
		// Transport level metrics.
		duration = prometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "relay",
			Name:      "request_duration_ns",
			Help:      "Request duration in nanoseconds.",
		}, []string{"method", "success"})
	}

	// Tracing domain.
	var tracer stdopentracing.Tracer
	{
		if *appdashAddr != "" {
			logger := log.NewContext(logger).With("tracer", "Appdash")
			logger.Log("addr", *appdashAddr)
			tracer = appdashot.NewTracer(appdash.NewRemoteCollector(*appdashAddr))
		} else {
			logger := log.NewContext(logger).With("tracer", "none")
			logger.Log()
			tracer = stdopentracing.GlobalTracer() // no-op
		}
	}

	// Business domain.
	var service relay.Service
	{
		service = relay.NewBasicService(*ircNick, *ircHost, *ircSecret)
		service = relay.ServiceLoggingMiddleware(logger)(service)
		service = relay.ServiceInstrumentingMiddleware(cx, cxerr, joins, chats, names)(service)
	}

	// Endpoint domain.
	var connectEndpoint endpoint.Endpoint
	{
		connectDuration := duration.With("method", "Connect")
		connectLogger := log.NewContext(logger).With("method", "Connect")

		connectEndpoint = relay.MakeConnectEndpoint(service)
		connectEndpoint = opentracing.TraceServer(tracer, "Connect")(connectEndpoint)
		connectEndpoint = relay.EndpointInstrumentingMiddleware(connectDuration)(connectEndpoint)
		connectEndpoint = relay.EndpointLoggingMiddleware(connectLogger)(connectEndpoint)
	}
	var joinEndpoint endpoint.Endpoint
	{
		joinDuration := duration.With("method", "Join")
		joinLogger := log.NewContext(logger).With("method", "Join")

		joinEndpoint = relay.MakeJoinEndpoint(service)
		joinEndpoint = opentracing.TraceServer(tracer, "Join")(joinEndpoint)
		joinEndpoint = relay.EndpointInstrumentingMiddleware(joinDuration)(joinEndpoint)
		joinEndpoint = relay.EndpointLoggingMiddleware(joinLogger)(joinEndpoint)
	}
	var chatEndpoint endpoint.Endpoint
	{
		chatDuration := duration.With("method", "Chat")
		chatLogger := log.NewContext(logger).With("method", "Chat")

		chatEndpoint = relay.MakeChatEndpoint(service)
		chatEndpoint = opentracing.TraceServer(tracer, "Chat")(chatEndpoint)
		chatEndpoint = relay.EndpointInstrumentingMiddleware(chatDuration)(chatEndpoint)
		chatEndpoint = relay.EndpointLoggingMiddleware(chatLogger)(chatEndpoint)
	}
	var namesEndpoint endpoint.Endpoint
	{
		namesDuration := duration.With("method", "Names")
		namesLogger := log.NewContext(logger).With("method", "Names")

		namesEndpoint = relay.MakeNamesEndpoint(service)
		namesEndpoint = opentracing.TraceServer(tracer, "Names")(namesEndpoint)
		namesEndpoint = relay.EndpointInstrumentingMiddleware(namesDuration)(namesEndpoint)
		namesEndpoint = relay.EndpointLoggingMiddleware(namesLogger)(namesEndpoint)
	}
	endpoints := relay.Endpoints{
		ConnectEndpoint: connectEndpoint,
		JoinEndpoint:    joinEndpoint,
		ChatEndpoint:    chatEndpoint,
		NamesEndpoint:   namesEndpoint,
	}

	// Mechanical domain.
	errc := make(chan error)
	ctx := context.Background()

	// Interrupt handler.
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// Debug listener.
	go func() {
		logger := log.NewContext(logger).With("transport", "debug")

		m := http.NewServeMux()
		m.Handle("/debug/pprof/", http.HandlerFunc(pprof.Index))
		m.Handle("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
		m.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
		m.Handle("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
		m.Handle("/debug/pprof/trace", http.HandlerFunc(pprof.Trace))
		m.Handle("/metrics", stdprometheus.Handler())

		logger.Log("addr", *debugAddr)
		errc <- http.ListenAndServe(*debugAddr, m)
	}()

	// gRPC transport.
	go func() {
		logger := log.NewContext(logger).With("transport", "gRPC")

		ln, err := net.Listen("tcp", *grpcAddr)
		if err != nil {
			errc <- err
			return
		}

		srv := relay.MakeGRPCServer(ctx, endpoints, tracer, logger)
		s := grpc.NewServer()
		pb.RegisterRelayServer(s, srv)

		logger.Log("addr", *grpcAddr)
		errc <- s.Serve(ln)
	}()

	// Run!
	logger.Log("exit", <-errc)
}
