// Package relay is an IRC microservice. It can connect to IRC hosts and
// join channels. A client library is available in the client
// subdirectory. A server binary is available in cmd/relaysvc. An example client
// binary is available in cmd/relaycli.
package relay
