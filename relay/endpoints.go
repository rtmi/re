package relay

// This file contains methods to make individual endpoints from services,
// request and response types to serve those endpoints, as well as encoders and
// decoders for those types, for all of our supported transport serialization
// formats. It also includes endpoint middlewares.

import (
	"fmt"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
)

// Endpoints collects all of the endpoints that compose an relay service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
//
// In a server, it's useful for functions that need to operate on a per-endpoint
// basis. For example, you might pass an Endpoints to a function that produces
// an http.Handler, with each method (endpoint) wired up to a specific path. (It
// is probably a mistake in design to invoke the Service methods on the
// Endpoints struct in a server.)
//
// In a client, it's useful to collect individually constructed endpoints into a
// single type that implements the Service interface. For example, you might
// construct individual endpoints using transport/http.NewClient, combine them
// into an Endpoints, and return it to the caller as a Service.
type Endpoints struct {
	ConnectEndpoint endpoint.Endpoint
	JoinEndpoint    endpoint.Endpoint
	ChatEndpoint    endpoint.Endpoint
	NamesEndpoint   endpoint.Endpoint
}

// Connect implements Service. Primarily useful in a client.
func (e Endpoints) Connect(ctx context.Context, s string) (string, error) {
	request := connectRequest{S: s}
	response, err := e.ConnectEndpoint(ctx, request)
	if err != nil {
		return "", err
	}
	return response.(connectResponse).V, response.(connectResponse).Err
}

// Join implements Service. Primarily useful in a client.
func (e Endpoints) Join(ctx context.Context, c string) (string, error) {
	request := joinRequest{C: c}
	response, err := e.JoinEndpoint(ctx, request)
	if err != nil {
		return "", err
	}
	return response.(joinResponse).V, response.(joinResponse).Err
}

func (e Endpoints) Chat(ctx context.Context, c string, m string) error {
	request := chatRequest{C: c, M: m}
	response, err := e.ChatEndpoint(ctx, request)
	if err != nil {
		return err
	}
	return response.(chatResponse).Err
}

func (e Endpoints) Names(ctx context.Context, c string) error {
	request := namesRequest{C: c}
	response, err := e.NamesEndpoint(ctx, request)
	if err != nil {
		return err
	}
	return response.(namesResponse).Err
}

// MakeConnectEndpoint returns an endpoint that invokes Connect on the service.
// Primarily useful in a server.
func MakeConnectEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		connectReq := request.(connectRequest)
		v, err := s.Connect(ctx, connectReq.S)
		if err != nil {
			return nil, err
		}
		return connectResponse{
			V:   v,
			Err: err,
		}, nil
	}
}

// MakeJoinEndpoint returns an endpoint that invokes Join on the service.
// Primarily useful in a server.
func MakeJoinEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		joinReq := request.(joinRequest)
		v, err := s.Join(ctx, joinReq.C)
		return joinResponse{
			V:   v,
			Err: err,
		}, nil
	}
}

func MakeChatEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		chatReq := request.(chatRequest)
		err = s.Chat(ctx, chatReq.C, chatReq.M)
		return chatResponse{
			Err: err,
		}, nil
	}
}

func MakeNamesEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		namesReq := request.(namesRequest)
		err = s.Names(ctx, namesReq.C)
		return namesResponse{
			Err: err,
		}, nil
	}
}

// EndpointInstrumentingMiddleware returns an endpoint middleware that records
// the duration of each invocation to the passed histogram. The middleware adds
// a single field: "success", which is "true" if no error is returned, and
// "false" otherwise.
func EndpointInstrumentingMiddleware(duration metrics.Histogram) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {

			defer func(begin time.Time) {
				duration.With("success", fmt.Sprint(err == nil)).Observe(time.Since(begin).Seconds())
			}(time.Now())
			return next(ctx, request)

		}
	}
}

// EndpointLoggingMiddleware returns an endpoint middleware that logs the
// duration of each invocation, and the resulting error, if any.
func EndpointLoggingMiddleware(logger log.Logger) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {

			defer func(begin time.Time) {
				logger.Log("error", err, "took", time.Since(begin))
			}(time.Now())
			return next(ctx, request)

		}
	}
}

// These types are unexported because they only exist to serve the endpoint
// domain, which is totally encapsulated in this package. They are otherwise
// opaque to all callers.

type connectRequest struct{ S string }

type connectResponse struct {
	V   string
	Err error
}

type joinRequest struct{ C string }

type joinResponse struct {
	V   string
	Err error
}

type chatRequest struct{ C, M string }

type chatResponse struct {
	Err error
}

type namesRequest struct{ C string }

type namesResponse struct {
	Err error
}
