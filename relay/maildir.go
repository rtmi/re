package relay

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"log"
	"net"
	"os"
	"os/user"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/patterns/irc"
	"github.com/patterns/maildir"
)

type Redir struct {
	// Encapsulates the current IRC server connection
	Server  string
	Maildir maildir.Dir
}

func formatservername(name string) string {
	// Takes the IRC server name and returns only alphanumerics
	host, _, _ := net.SplitHostPort(name)
	reg, err := regexp.Compile("[^A-Za-z0-9]+")
	if err != nil {
		log.Fatal(err)
	}

	safe := reg.ReplaceAllString(host, "")
	safe = "." + strings.ToLower(strings.Trim(safe, " "))

	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	full := path.Join(usr.HomeDir, safe)
	return full
}

func exists(path string) bool {
	// "Borrowed" file-exist check
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	panic(err)
}

func NewRedir(srv string) *Redir {
	// Instantiate our MaildIRC abstraction obj
	normname := formatservername(srv)
	var d maildir.Dir = maildir.Dir(normname)

	if !exists(normname) {
		fmt.Printf("creating maildir (%s) \n", normname)
		err := d.Create()
		if err != nil {
			log.Fatal(err)
		}
	}

	host := strings.Split(srv, ":")
	m := &Redir{
		Server:  host[0],
		Maildir: d,
	}
	return m
}

func (p *Redir) Save(m *irc.Message) error {
	if m == nil {
		return nil
	}
	msghd := fmt.Sprintf("From: %v\nSender: %v@%v\nTo: %v\nSubject: %v\nDate: %v\nKeywords: %v\nComments: %v",
		formatFrom(m),
		formatSender(m),
		formatSenderat(m),
		p.Server,
		formatChannel(m),
		time.Now().UTC().Format(time.RFC1123Z),
		m.Command,
		formatComments(m))

	chkdata := []byte(msghd + m.Trailing)
	sum := md5.Sum(chkdata)
	msg := fmt.Sprintf("%v\nMessage-ID: %v\n\n%v",
		msghd,
		hex.EncodeToString(sum[:]),
		m.Trailing)

	dlv, err := p.Maildir.NewDelivery()
	if err != nil {
		return err
	}

	_, err = dlv.Write([]byte(msg))
	if err != nil {
		return err
	}

	err = dlv.Close()
	if err != nil {
		return err
	}
	return nil
}

func formatFrom(m *irc.Message) string {
	if m.Prefix != nil &&
		len(m.Prefix.User) != 0 {
		return m.Prefix.User
	}
	return ""
}

func formatSender(m *irc.Message) string {
	if m.Prefix != nil &&
		len(m.Prefix.Name) != 0 {
		return m.Prefix.Name
	}
	return ""
}

func formatSenderat(m *irc.Message) string {
	if m.Prefix != nil &&
		len(m.Prefix.Host) != 0 {
		return m.Prefix.Host
	}
	return ""
}

//TODO I think it is easier to encode the IRC tags into json
func formatComments(m *irc.Message) string {
	var csv string = ""
	if len(m.Params) != 0 {
		csv = strings.Join(m.Params, ",")
	}

	if len(m.Tags) != 0 {
		test := ""
		for k, v := range m.Tags {
			test = test + ";" + k + "=" + strings.Replace(v, "\x0a", "lf", -1)
		}
		csv = csv + test
	}

	return csv
}

func formatChannel(m *irc.Message) string {
	// Until we study the fields, only PRIVMSG are mapped to channels.
	// Others will be lumped into a "Verbose" channel.
	if m.Command == "PRIVMSG" &&
		len(m.Params) != 0 {
		return m.Params[0]
	}

	return "verbose"
}
