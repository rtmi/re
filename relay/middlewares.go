package relay

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
)

// Middleware describes a service (as opposed to endpoint) middleware.
type Middleware func(Service) Service

// ServiceLoggingMiddleware returns a service middleware that logs the
// parameters and result of each method invocation.
func ServiceLoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return serviceLoggingMiddleware{
			logger: logger,
			next:   next,
		}
	}
}

type serviceLoggingMiddleware struct {
	logger log.Logger
	next   Service
}

func (mw serviceLoggingMiddleware) Connect(ctx context.Context, s string) (v string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "Connect",
			"s", s, "result", v, "error", err,
			"took", time.Since(begin),
		)
	}(time.Now())
	return mw.next.Connect(ctx, s)
}

func (mw serviceLoggingMiddleware) Join(ctx context.Context, c string) (v string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "Join",
			"c", c, "result", v, "error", err,
			"took", time.Since(begin),
		)
	}(time.Now())
	return mw.next.Join(ctx, c)
}

func (mw serviceLoggingMiddleware) Chat(ctx context.Context, c string, m string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "Chat",
			"c", c, "m", m, "error", err,
			"took", time.Since(begin),
		)
	}(time.Now())
	return mw.next.Chat(ctx, c, m)
}

func (mw serviceLoggingMiddleware) Names(ctx context.Context, c string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "Names",
			"c", c, "error", err,
			"took", time.Since(begin),
		)
	}(time.Now())
	return mw.next.Names(ctx, c)
}

// ServiceInstrumentingMiddleware returns a service middleware that instruments
// the number of connections, joins, and chats over the lifetime of
// the service.
func ServiceInstrumentingMiddleware(cx, cxerr, joins, chats, names metrics.Counter) Middleware {
	return func(next Service) Service {
		return serviceInstrumentingMiddleware{
			cx:    cx,
			cxerr: cxerr,
			joins: joins,
			chats: chats,
			names: names,
			next:  next,
		}
	}
}

type serviceInstrumentingMiddleware struct {
	cx    metrics.Counter
	cxerr metrics.Counter
	joins metrics.Counter
	chats metrics.Counter
	names metrics.Counter
	next  Service
}

func (mw serviceInstrumentingMiddleware) Connect(ctx context.Context, s string) (string, error) {
	v, err := mw.next.Connect(ctx, s)
	if v == "ConnectionLive" {
		mw.cx.Add(float64(1))
	} else {
		mw.cxerr.Add(float64(1))
	}
	return v, err
}

func (mw serviceInstrumentingMiddleware) Join(ctx context.Context, c string) (string, error) {
	v, err := mw.next.Join(ctx, c)
	mw.joins.Add(float64(1))
	return v, err
}

func (mw serviceInstrumentingMiddleware) Chat(ctx context.Context, c string, m string) error {
	err := mw.next.Chat(ctx, c, m)
	mw.chats.Add(float64(1))
	return err
}

func (mw serviceInstrumentingMiddleware) Names(ctx context.Context, c string) error {
	err := mw.next.Names(ctx, c)
	mw.names.Add(float64(1))
	return err
}
