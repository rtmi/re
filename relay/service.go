package relay

// This file contains the Service definition, and a basic service
// implementation.

import (
	"errors"
	"strings"
	"sync"

	"github.com/patterns/irc"

	"golang.org/x/net/context"
)

// Service describes a service that saves IRC to local maildir store.
type Service interface {
	Connect(ctx context.Context, s string) (string, error)
	Join(ctx context.Context, c string) (string, error)
	Chat(ctx context.Context, c string, m string) error
	Names(ctx context.Context, c string) error
}

// Business-domain errors like these may be served in two ways: returned
// directly by endpoints, or bundled into the response struct. Both methods can
// be made to work, but errors returned directly by endpoints are counted by
// middlewares that check errors, like circuit breakers.
//
// If you don't want that behavior -- and you probably don't -- then it's better
// to bundle errors into the response struct.

var (
	// ErrIrcConnected is returned when a live connection exists.
	ErrIrcConnected = errors.New("Connection Exists, disconnect first before making another connect call.")

	// ErrChannelJoined is returned when a join request is repeated on the same channel.
	ErrChannelJoined = errors.New("Current Channel, part channel first before making another join call.")

	// ErrDetached is returned when a chat request is made outside channel.
	ErrDetached = errors.New("Chat Limbo, join channel first before chatting.")

	// ErrMaxSizeExceeded protects the Concat method.
	ErrMaxSizeExceeded = errors.New("result exceeds maximum size")
)

// These annoying helper functions are required to translate Go error types to
// and from strings, which is the type we use in our IDLs to represent errors.
// There is special casing to treat empty strings as nil errors.

func str2err(s string) error {
	if s == "" {
		return nil
	}
	return errors.New(s)
}

func err2str(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}

// NewBasicService returns a implementation of Service.
func NewBasicService(nick string, srv string, secret string) Service {
	return &basicService{
		state:    "Created",
		nickname: nick,
		server:   srv,
		secret:   secret,
		channels: safemap{m: make(map[string]int)},
		outbuff:  make(chan irc.Message, 100),
	}
}

type safemap struct {
	sync.RWMutex
	m map[string]int
}

type basicService struct {
	state      string
	nickname   string
	server     string
	secret     string
	channels   safemap
	connection *irc.Conn
	outbuff    chan irc.Message
	dir        *Redir
}

// Connect implements Service.
func (r *basicService) Connect(_ context.Context, srv string) (string, error) {
	r.channels.Lock()
	count := r.channels.m[srv]
	if count != 0 {
		r.channels.Unlock()
		return "", ErrIrcConnected
	}

	c, err := irc.Dial(srv)
	if err != nil {
		r.channels.Unlock()
		return "", err
	}
	r.channels.m[srv]++
	r.channels.Unlock()
	r.state = "ConnectionLive"

	r.connection = c
	r.server = srv
	r.dir = NewRedir(srv)
	go r.outbound()
	go r.inbound()
	r.nick(r.secret)
	r.capability()

	return r.state, nil
}

// Join implements Service.
func (r *basicService) Join(_ context.Context, ch string) (string, error) {
	//TODO Can we check against a list of valid channel names on the server before attempt?
	low := strings.ToLower(ch)
	r.channels.Lock()
	count := r.channels.m[low]
	if count != 0 {
		r.channels.Unlock()
		return "", ErrChannelJoined
	}

	r.join(low)
	r.channels.m[low]++
	r.channels.Unlock()

	return low, nil
}

func (r *basicService) Chat(_ context.Context, ch string, m string) error {
	low := strings.ToLower(ch)
	r.channels.RLock()
	defer r.channels.RUnlock()
	count := r.channels.m[low]
	if count == 0 {
		return ErrDetached
	}

	r.chat(low, m)
	return nil
}

func (r *basicService) Names(_ context.Context, ch string) error {
	low := strings.ToLower(ch)
	r.channels.RLock()
	defer r.channels.RUnlock()
	count := r.channels.m[low]
	if count == 0 {
		return ErrDetached
	}

	r.names(low)
	return nil
}

func (r *basicService) inbound() {
	for {
		msg, err := r.connection.Decode()
		if err != nil {
			panic(err)
		}
		err = r.dispatch(msg)
		if err != nil {
			panic(err)
		}
	}
}

func (r *basicService) outbound() {
	for msg := range r.outbuff {
		r.connection.Encode(&msg)
	}
}

func (r *basicService) dispatch(msg *irc.Message) error {
	switch msg.Command {
	case irc.PING:
		r.pong(msg.Trailing)
	default:
		return r.dir.Save(msg)
	}
	return nil
}

func (r *basicService) names(ch string) {
	r.outbuff <- irc.Message{
		Command: irc.NAMES,
		Params:  []string{ch},
	}
}

func (r *basicService) chat(ch string, m string) {
	r.outbuff <- irc.Message{
		Command:  irc.PRIVMSG,
		Params:   []string{ch},
		Trailing: m,
	}
}

func (r *basicService) join(ch string) {
	r.outbuff <- irc.Message{
		Command: irc.JOIN,
		Params:  []string{ch},
	}
}

func (r *basicService) capability() {
	if !strings.Contains(r.server, "twitch.tv") {
		return
	}
	r.outbuff <- irc.Message{
		Command:  irc.CAP,
		Params:   []string{"REQ"},
		Trailing: "twitch.tv/membership",
	}
	r.outbuff <- irc.Message{
		Command:  irc.CAP,
		Params:   []string{"REQ"},
		Trailing: "twitch.tv/commands",
	}
	r.outbuff <- irc.Message{
		Command:  irc.CAP,
		Params:   []string{"REQ"},
		Trailing: "twitch.tv/tags",
	}
}

func (r *basicService) nick(pass string) {
	if len(pass) != 0 {
		r.outbuff <- irc.Message{
			Command: irc.PASS,
			Params:  []string{pass},
		}
	}
	r.outbuff <- irc.Message{
		Command: irc.NICK,
		Params:  []string{r.nickname},
	}
	r.outbuff <- irc.Message{
		Command:  irc.USER,
		Params:   []string{r.nickname, "0", "*"},
		Trailing: r.nickname,
	}
}

func (r *basicService) pong(pingdata string) {
	pongBack := irc.Message{
		Command:  irc.PONG,
		Trailing: pingdata,
	}
	r.outbuff <- pongBack
}
