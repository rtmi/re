package relay

// This file provides server-side bindings for the gRPC transport.
// It utilizes the transport/grpc.Server.

import (
	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	"github.com/patterns/re/relay/pb"
)

// MakeGRPCServer makes a set of endpoints available as a gRPC AddServer.
func MakeGRPCServer(ctx context.Context, endpoints Endpoints, tracer stdopentracing.Tracer, logger log.Logger) pb.RelayServer {
	options := []grpctransport.ServerOption{
		grpctransport.ServerErrorLogger(logger),
	}
	return &grpcServer{
		connect: grpctransport.NewServer(
			ctx,
			endpoints.ConnectEndpoint,
			DecodeGRPCConnectRequest,
			EncodeGRPCConnectResponse,
			append(options, grpctransport.ServerBefore(opentracing.FromGRPCRequest(tracer, "Connect", logger)))...,
		),
		join: grpctransport.NewServer(
			ctx,
			endpoints.JoinEndpoint,
			DecodeGRPCJoinRequest,
			EncodeGRPCJoinResponse,
			append(options, grpctransport.ServerBefore(opentracing.FromGRPCRequest(tracer, "Join", logger)))...,
		),
		chat: grpctransport.NewServer(
			ctx,
			endpoints.ChatEndpoint,
			DecodeGRPCChatRequest,
			EncodeGRPCChatResponse,
			append(options, grpctransport.ServerBefore(opentracing.FromGRPCRequest(tracer, "Chat", logger)))...,
		),
		names: grpctransport.NewServer(
			ctx,
			endpoints.NamesEndpoint,
			DecodeGRPCNamesRequest,
			EncodeGRPCNamesResponse,
			append(options, grpctransport.ServerBefore(opentracing.FromGRPCRequest(tracer, "Names", logger)))...,
		),
	}
}

type grpcServer struct {
	connect grpctransport.Handler
	join    grpctransport.Handler
	chat    grpctransport.Handler
	names   grpctransport.Handler
}

func (s *grpcServer) Connect(ctx context.Context, req *pb.ConnectRequest) (*pb.ConnectReply, error) {
	_, rep, err := s.connect.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ConnectReply), nil
}

func (s *grpcServer) Join(ctx context.Context, req *pb.JoinRequest) (*pb.JoinReply, error) {
	_, rep, err := s.join.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.JoinReply), nil
}

func (s *grpcServer) Chat(ctx context.Context, req *pb.ChatRequest) (*pb.ChatReply, error) {
	_, rep, err := s.chat.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ChatReply), nil
}

func (s *grpcServer) Names(ctx context.Context, req *pb.NamesRequest) (*pb.NamesReply, error) {
	_, rep, err := s.names.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.NamesReply), nil
}

// DecodeGRPCConnectRequest is a transport/grpc.DecodeRequestFunc that converts a
// gRPC connect request to a user-domain connect request. Primarily useful in a server.
func DecodeGRPCConnectRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.ConnectRequest)
	return connectRequest{S: req.S}, nil
}

// DecodeGRPCJoinRequest is a transport/grpc.DecodeRequestFunc that converts a
// gRPC join request to a user-domain join request. Primarily useful in a
// server.
func DecodeGRPCJoinRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.JoinRequest)
	return joinRequest{C: req.C}, nil
}

func DecodeGRPCChatRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.ChatRequest)
	return chatRequest{C: req.C, M: req.M}, nil
}

func DecodeGRPCNamesRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.NamesRequest)
	return namesRequest{C: req.C}, nil
}

// DecodeGRPCConnectResponse is a transport/grpc.DecodeResponseFunc that converts a
// gRPC connect reply to a user-domain connect response. Primarily useful in a client.
func DecodeGRPCConnectResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	reply := grpcReply.(*pb.ConnectReply)
	return connectResponse{V: reply.V, Err: str2err(reply.Err)}, nil
}

// DecodeGRPCJoinResponse is a transport/grpc.DecodeResponseFunc that converts
// a gRPC join reply to a user-domain join response. Primarily useful in a
// client.
func DecodeGRPCJoinResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	reply := grpcReply.(*pb.JoinReply)
	return joinResponse{V: reply.V, Err: str2err(reply.Err)}, nil
}

func DecodeGRPCChatResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	reply := grpcReply.(*pb.ChatReply)
	return chatResponse{Err: str2err(reply.Err)}, nil
}

func DecodeGRPCNamesResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	reply := grpcReply.(*pb.NamesReply)
	return namesResponse{Err: str2err(reply.Err)}, nil
}

// EncodeGRPCConnectResponse is a transport/grpc.EncodeResponseFunc that converts a
// user-domain connect response to a gRPC connect reply. Primarily useful in a server.
func EncodeGRPCConnectResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(connectResponse)
	return &pb.ConnectReply{V: resp.V, Err: err2str(resp.Err)}, nil
}

// EncodeGRPCJoinResponse is a transport/grpc.EncodeResponseFunc that converts
// a user-domain join response to a gRPC join reply. Primarily useful in a
// server.
func EncodeGRPCJoinResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(joinResponse)
	return &pb.JoinReply{V: resp.V, Err: err2str(resp.Err)}, nil
}

func EncodeGRPCChatResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(chatResponse)
	return &pb.ChatReply{Err: err2str(resp.Err)}, nil
}

func EncodeGRPCNamesResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(namesResponse)
	return &pb.NamesReply{Err: err2str(resp.Err)}, nil
}

// EncodeGRPCConnectRequest is a transport/grpc.EncodeRequestFunc that converts a
// user-domain connect request to a gRPC connect request. Primarily useful in a client.
func EncodeGRPCConnectRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(connectRequest)
	return &pb.ConnectRequest{S: req.S}, nil
}

// EncodeGRPCJoinRequest is a transport/grpc.EncodeRequestFunc that converts a
// user-domain join request to a gRPC join request. Primarily useful in a
// client.
func EncodeGRPCJoinRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(joinRequest)
	return &pb.JoinRequest{C: req.C}, nil
}

func EncodeGRPCChatRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(chatRequest)
	return &pb.ChatRequest{C: req.C, M: req.M}, nil
}

func EncodeGRPCNamesRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(namesRequest)
	return &pb.NamesRequest{C: req.C}, nil
}
