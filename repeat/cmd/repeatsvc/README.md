# Repeatsvc

Repeatsvc is a polling svc to perform repetitive tasks.
Currenlty only handles Twitch follow/host polling, and attendance.

## Running / Development

* `repeatsvc -grpc.relay=localhost:8002 -http.rest=localhost:8080 -caster=overwatchopen -clientid=zzz -maildir=/Users/patterns/.ircchattwitchtv -bdb=/Users/patterns/overwatchopen.bdb`

## Further Reading / Useful Links

* [Twitch API](https://dev.twitch.tv/docs/api/v3/follows/)



