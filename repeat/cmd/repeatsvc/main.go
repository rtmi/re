package main

import (
	"flag"
	//	"fmt"
	//	"github.com/go-kit/kit/log"
	//	stdopentracing "github.com/opentracing/opentracing-go"
	//	"golang.org/x/net/context"
	//	"google.golang.org/grpc"
	//	"os"
	//	"strings"

	//	"github.com/patterns/re/relay"
	//	grpcclient "github.com/patterns/re/relay/client/grpc"
	"github.com/patterns/re/repeat"
	//	"github.com/patterns/re/rest"
	//	restclient "github.com/patterns/re/rest/client/http"
)

var (
	caster   = flag.String("caster", "overwatchopen", "Twitch broadcaster to follow alerts")
	clientid = flag.String("clientid", "", "Twitch dev app client ID")
	grpcAddr = flag.String("grpc.relay", "", "gRPC address of relaysvc")
	httpRest = flag.String("http.rest", "", "HTTP address of restsvc")
	maildir  = flag.String("maildir", ".Maildir", "Maildir directory path")
	bdb      = flag.String("bdb", "local.bdb", "BoltDB file path")
)

func main() {

	flag.Parse()
	s := repeat.NewService(*clientid, *maildir, *grpcAddr, *httpRest, *bdb)
	defer s.Shutdown()

	//TODO custom scripts need to be implemented as middleware?
	s.MakeHandler("!old", repeat.StandardScript, repeat.TotalVisitHandler)
	s.MakeHandler("!belay", repeat.StandardScript, repeat.BelayClipHandler)
	////	s.MakeHandler("!bank", repeat.StandardScript, repeat.PointsHandler)
	////	s.MakeHandler("!vegas", repeat.StandardScript, repeat.GambleHandler)

	s.Poll(*caster)
}
