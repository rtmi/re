package repeat

import (
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"net/url"
	"strings"
	"time"

	"github.com/patterns/re/relay"
	grpcclient "github.com/patterns/re/relay/client/grpc"
	"github.com/patterns/re/twitch"
)

// ResetFollows clears the tracked follows from the current runtime session.
func (s *twitchService) resetFollows(id string, name string) {
	c := caster{
		ID:         id,
		LastFollow: time.Now().UTC().Add(time.Second * -30),
		//DEBUG DEBUG		LastFollow:      time.Time{},
		RecentFollowers: []string{},
		Channel:         casterToIrcChannel(name),
	}
	s.c[id] = c
}

// HarvestFollows detects visitors that are new followers.
func (s *twitchService) harvestFollows(id string) ([]ChatJob, error) {
	//Send GET request
	//iterating as specified in response cursor/links.net
	//use DESC order and stop when timestamp values are older than LastFollow
	s.mtx.RLock()
	ch, ok := s.c[id]
	s.mtx.RUnlock()
	if !ok {
		return []ChatJob{}, errors.New("Channel is not initialized")
	}
	api := fmt.Sprintf("https://api.twitch.tv/kraken/channels/%s/follows", url.QueryEscape(ch.ID))
	d, err := twitch.Get(s.clientID, api)
	if err != nil {
		return []ChatJob{}, err
	}

	var fr twitch.FollowsResponse
	err = d.Decode(&fr)
	if err != nil {
		return []ChatJob{}, err
	}

	sz := len(fr.Follows)
	if sz == 0 {
		return []ChatJob{}, nil
	}

	nf := make([]ChatJob, 0, sz)
	for _, v := range fr.Follows {

		if ch.LastFollow.After(v.CreatedAt) || ch.LastFollow.Equal(v.CreatedAt) {
			break
		}

		j := ChatJob{Model: "chat"}

		j.Attributes.Body = v.User.DisplayName
		j.Attributes.Who = v.User.Name
		j.Attributes.Channel = ch.Channel
		j.Attributes.Command = "newfollow"

		nf = append(nf, j)
	}

	ch.LastFollow = fr.Follows[0].CreatedAt
	s.mtx.Lock()
	s.c[id] = ch
	s.mtx.Unlock()

	return nf, nil
}

// broadcastFollow announce the new follower to the chat channel.
func (s *twitchService) broadcastFollow(j ChatJob) error {

	var (
		notify string
		svc    relay.Service
		err    error
	)

	//TODO let main pass-in the external services
	conn, err := grpc.Dial(s.grpcAddr, grpc.WithInsecure(), grpc.WithTimeout(time.Second))
	if err != nil {
		return err
	}
	defer conn.Close()

	tracer := stdopentracing.GlobalTracer()
	svc = grpcclient.New(conn, tracer, log.NewNopLogger())

	notify = formatFollowNotify(j.Attributes.Who)

	if len(notify) != 0 {
		err := svc.Chat(context.Background(), j.Attributes.Channel, notify)
		if err != nil {
			return err
		}
	}

	return nil
}

// stampFollow record the follow timestamp.
func (s *twitchService) stampFollow(j ChatJob) error {

	//TODO the timestamp is already recorded by Twitch
	// why do we want to keep it when we can query for it?
	// If we do, overwrite the FirstVisit field with the follow stamp.
	return nil
}

func formatFollowNotify(f string) string {

	sprk := "\xE2\x9C\xA8"
	aln := "\xF0\x9F\x91\xBE"
////	sno := "\xE2\x9D\x84"
	em := "༼ つ ◎‿⊙༽つ\xF0\x9F\x8D\xA4"
	return fmt.Sprintf("/me %s a wild %s appears PogChamp can it be a new %s mutation %s", em, f, sprk, aln)
}

func casterToIrcChannel(c string) string {

	if strings.HasPrefix(c, "#") {
		return strings.ToLower(c)
	}

	return "#" + strings.ToLower(c)
}
