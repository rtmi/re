package repeat

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/patterns/re/record"
	"github.com/patterns/re/twitch"
)

type twitchService struct {
	mtx          sync.RWMutex
	c            map[string]caster
	h            map[int]int
	s            map[string]script
	clientID     string
	maildir      string
	grpcAddr     string
	httpRest     string
	bdb          *Store
	runTimestamp time.Time
}

type caster struct {
	ID              string
	LastFollow      time.Time
	RecentFollowers []string
	Channel         string
}

func NewService(clientid string,
	maildir string,
	grpcAddr string,
	httpRest string,
	bdb string) *twitchService {

	s := &twitchService{
		c:        map[string]caster{},
		h:        map[int]int{},
		s:        map[string]script{},
		clientID: clientid,
		maildir:  maildir,
		grpcAddr: grpcAddr,
		httpRest: httpRest,
	}

	d, err := s.openStore(bdb)
	if err != nil {
		panic(err)
	}

	s.bdb = d
	s.runTimestamp = time.Now().UTC()

	return s
}

func (s *twitchService) Poll(caster string) {

	var delay <-chan time.Time
	delay = time.Tick(5 * time.Minute)
	var hop <-chan time.Time
	hop = time.Tick(30 * time.Second)
	var skip <-chan time.Time
	skip = time.Tick(7 * time.Second)
	var tick <-chan time.Time
	tick = time.Tick(500 * time.Millisecond)
	jobs := make(chan ChatJob)

	id, err := s.usernameToID(caster)
	if err != nil {
		panic(err)
	}

	s.resetFollows(id, caster)
	s.predefinedScripts()

	for {
		select {
		case <-delay:
			/*			go func() {
						}()*/

		case <-hop:
			go func() {
				j, err := s.harvestFollows(id)
				if err != nil {
					panic(err)
				}
				if len(j) != 0 {
					for _, v := range j {
						jobs <- v
					}
				}
			}()

		case <-skip:
			go func() {
				j, err := s.harvestHosts(id)
				if err != nil {
					panic(err)
				}
				if len(j) != 0 {
					for _, v := range j {
						jobs <- v
					}
				}
			}()

		case <-tick:
			go func() {
				j, err := s.harvestChats()
				if err != nil {
					panic(err)
				}
				if len(j) != 0 {
					for _, v := range j {
						jobs <- v
					}
				}
			}()

		case j := <-jobs:
			go func() {
				err := s.handleEvents(j)
				if err != nil {
					panic(err)
				}
			}()
		}
	}

}

// handleEvents takes a chat datum and routes to event handlers.
func (s *twitchService) handleEvents(j ChatJob) error {

	err := s.cacheChat(j)
	if err != nil {
		//todo not fatal, log+continue
		fmt.Fprintf(os.Stderr, "REST svc- %s\n", err.Error())
	}

	err = s.calculateVisit(j)
	if err != nil {
		return err
	}

	switch strings.ToLower(j.Attributes.Command) {

	case "newfollow":
		_ = s.stampFollow(j)
		err = s.broadcastFollow(j)
		if err != nil {
			return err
		}
		return playSound("follow.wav")

	case "hosttarget":
		err = s.broadcastHost(j)
		if err != nil {
			return err
		}
		return playSound("host.wav")

	case "privmsg":
		return s.runCustomScripts(j)

	}

	return nil
}

func playSound(filename string) error {

	path, err := fullpath(filename)
	if err != nil {
		return err
	}

	err = record.Playback(path)
	if err != nil {
		return err
	}

	return nil
}

func fullpath(filename string) (string, error) {

	f, err := os.Getwd()
	if err != nil {
		return "", err
	}

	return path.Join(f, filename), nil
}

func (s *twitchService) IDToUsername(key int) (string, string, error) {
	api := fmt.Sprintf("https://api.twitch.tv/kraken/users/%v", key)
	d, err := twitch.Get(s.clientID, api)
	if err != nil {
		return "", "", err
	}
	var u twitch.User
	err = d.Decode(&u)
	if err != nil {
		return "", "", err
	}
	web := fmt.Sprintf("twitch.tv/%s", u.Name)
	return u.DisplayName, web, nil
}

func (s *twitchService) usernameToID(name string) (string, error) {
	api := fmt.Sprintf("https://api.twitch.tv/kraken/users?login=%s", url.QueryEscape(name))
	d, err := twitch.Get(s.clientID, api)
	if err != nil {
		return "", err
	}
	var ur twitch.UsersResponse
	err = d.Decode(&ur)
	if err != nil {
		return "", err
	}
	return ur.Users[0].ID, nil
}
