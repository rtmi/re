package repeat

import (
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"net/url"
	"strings"
	"time"

	"github.com/patterns/re/relay"
	grpcclient "github.com/patterns/re/relay/client/grpc"
	"github.com/patterns/re/twitch"
)

//TODO probably want to yield on first delta and avoid blasting API calls
func (s *twitchService) harvestHosts(id string) ([]ChatJob, error) {

	s.mtx.RLock()
	ch, ok := s.c[id]
	s.mtx.RUnlock()
	if !ok {
		return []ChatJob{}, errors.New("Channel is not initialized")
	}

	api := fmt.Sprintf("https://tmi.twitch.tv/hosts?include_login=1&target=%s", url.QueryEscape(id))

	d, err := twitch.Get(s.clientID, api)
	if err != nil {
		return []ChatJob{}, err
	}

	var hr twitch.HostsResponse
	err = d.Decode(&hr)
	if err != nil {
		return []ChatJob{}, err
	}

	sz := len(hr.Hosts)
	if sz == 0 {
		return []ChatJob{}, nil
	}

	keys := make([]int, 0, sz)
	s.mtx.Lock()
	for _, v := range hr.Hosts {

		// compare to previous list for deltas, new hosts will be
		if t, ok := s.h[v.HostID]; !ok {
			keys = append(keys, v.HostID)
			if v.HostPartnered {
				s.h[v.HostID] = 1
			} else {
				s.h[v.HostID] = 2
			}
		} else {
			//TODO expiry mgt
			t++ //mark as accessed recently
		}
	}
	s.mtx.Unlock()

	nh := make([]ChatJob, 0, len(keys))
	for _, v := range keys {

		friendly, web, err := s.IDToUsername(v)
		if err != nil {
			return []ChatJob{}, err
		}

		j := ChatJob{Model: "chat"}

		j.Attributes.Body = web
		j.Attributes.Who = friendly
		j.Attributes.Channel = ch.Channel
		j.Attributes.Command = "hosttarget"
		j.Attributes.Tags = fmt.Sprintf("%s;", friendly)

		nh = append(nh, j)
	}

	return nh, nil
}

// broadcastHost write the chat message via the grpc svc.
func (s *twitchService) broadcastHost(j ChatJob) error {

	var (
		notify string
		svc    relay.Service
		err    error
	)

	//TODO let main pass-in the external services
	conn, err := grpc.Dial(s.grpcAddr, grpc.WithInsecure(), grpc.WithTimeout(time.Second))
	if err != nil {
		return err
	}
	defer conn.Close()

	tracer := stdopentracing.GlobalTracer()
	svc = grpcclient.New(conn, tracer, log.NewNopLogger())

	notify = formatHostNotify(j.Attributes.Tags, j.Attributes.Body)

	if len(notify) != 0 {
		err := svc.Chat(context.Background(), j.Attributes.Channel, notify)
		if err != nil {
			return err
		}
	}

	return nil
}

func formatHostNotify(param string, body string) string {

	if strings.HasPrefix(body, "-") {
		fmt.Printf("host-end\n")
		return ""
	}

	l := strings.Split(param, ";")
	name := l[0]

	url := strings.ToUpper("twitch.tv/" + name)
	fmt.Printf("host- %s\n", url)

	prty := "\xF0\x9F\x8E\x89"
	drnk := "\xF0\x9F\x8D\xB9"
	em := "\xF0\x9F\x8D\xA4ς(◑ω◐)ɂ\xF0\x9F\x8D\xA4"

	return fmt.Sprintf("/me %s %s %s %s did a host, icanhaz happyhr now? everyone goto %s click teh thingz push all the buttons!", em, prty, name, drnk, url)
}
