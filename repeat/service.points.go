package repeat

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
	mdir "github.com/patterns/maildir"
	"github.com/patterns/re/rest"
	restclient "github.com/patterns/re/rest/client/http"
	"golang.org/x/net/context"
)

type ChatJob struct {
	Model      string
	ID         string
	Attributes struct {
		Body    string
		Who     string
		Channel string
		Host    string
		Command string
		Tags    string
	}
}

func (s *twitchService) openStore(bdb string) (*Store, error) {

	d := &Store{
		Path: bdb,
	}

	if err := d.Open(); err != nil {
		return d, err
	}

	return d, nil
}

// Shutdown releases resources managed by the svc
func (s *twitchService) Shutdown() {

	// Tally attendance with a final sweep
	rc, err := s.bdb.Members()
	if err != nil {

		fmt.Fprintf(os.Stderr, "ERROR Shutdown %v\n", err.Error())

	} else {

		for _, v := range rc {
			s.endVisit(v)
		}
	}

	s.bdb.Close()
}

// harvestChats opens the items from the maildir's unread/new queue.
func (s *twitchService) harvestChats() ([]ChatJob, error) {

	var err error

	d := mdir.Dir(s.maildir)

	keys, err := d.Unseen()
	if err != nil {
		return []ChatJob{}, err
	}
	if len(keys) == 0 {
		return []ChatJob{}, nil
	}

	j := make([]ChatJob, 0, len(keys))
	for _, v := range keys {

		m, err := d.Message(v)
		if err != nil {
			return []ChatJob{}, err
		}

		i, err := newChatJob(m.Header, m.Body)
		if err != nil {
			return []ChatJob{}, err
		}

		_ = d.SetFlags(v, "S")

		j = append(j, i)
	}

	return j, nil
}

// Shallow copy of the mail headers which ignores all but the first value of
// each header
func newChatJob(h map[string][]string, b io.Reader) (ChatJob, error) {

	var id, who, ch, srv, cmd, tag string
	for k, v := range h {

		switch strings.ToLower(k) {

		case "message-id":
			id = v[0]
		case "from":
			who = v[0]
		case "subject":
			ch = v[0]
		case "to":
			srv = v[0]
		case "keywords":
			cmd = v[0]
		case "comments":
			tag = v[0]
		}
	}

	s, err := ioutil.ReadAll(b)
	if err != nil {
		return ChatJob{}, err
	}

	j := ChatJob{Model: "chat", ID: id}

	j.Attributes.Body = string(s)
	j.Attributes.Who = who
	j.Attributes.Channel = ch
	j.Attributes.Host = srv
	j.Attributes.Command = cmd
	j.Attributes.Tags = tag

	return j, nil
}

//TODO track member activity
//     if count is 1000 and no members have parted, then no-op
func (s *twitchService) calculateVisit(j ChatJob) error {

	switch strings.ToLower(j.Attributes.Command) {

	case "part":

		u, err := s.findMember(j.Attributes.Who)
		if err != nil {
			return err
		}

		s.endVisit(u)

	case "join":

		u, err := s.getMember(j.Attributes.Who)
		if err != nil {
			return err
		}

		s.addVisit(u)
	}

	return nil
}

// getViewer retrieves named member or adds when missing.
func (s *twitchService) getMember(name string) (*User, error) {

	target := strings.ToLower(name)

	l, err := s.bdb.Member(target)
	if err != nil {
		return &User{}, err
	}

	if l == nil {
		l = newMember(target)
		s.bdb.CreateUser(l)
	}

	return l, nil
}

// findViewer locates the member by name and errors when missing.
func (s *twitchService) findMember(name string) (*User, error) {

	target := strings.ToLower(name)

	l, err := s.bdb.Member(target)
	if err != nil {
		return &User{}, err
	}

	if strings.ToLower(l.Username) != target {
		return &User{}, ErrUserNone
	}

	return l, nil
}

// hasElapsedFiveMins checks whether the user's last visit has been further
// than 5 mins in the past (it is considered invalid if last-visit is older
// than the current run start time because it probably indicates last
// shutdown did not clean-up)
func (s *twitchService) hasElapsedFiveMins(user *User) (bool, error) {

	lst, err := time.Parse(time.RFC3339, user.LastVisit)
	if err != nil {
		return false, err
	}

	// Zeros mean timestamp is for ended session
	if lst.Minute() == 0 && lst.Second() == 0 && lst.Nanosecond() == 0 {
		return false, nil
	}

	// Last timestamp is outside the current run start time
	if lst.Before(s.runTimestamp) {
		return false, nil
	}

	// Last timestamp is further than five mins into past
	if time.Now().UTC().After(lst.Add(time.Minute * 5)) {
		return true, nil
	}

	return false, nil
}

// addVisit adds the elapsed time to TotalVisit and refreshes LastVisit
func (s *twitchService) addVisit(user *User) error {

	activity, err := s.hasElapsedFiveMins(user)
	if err != nil {
		return err
	}

	lst, err := time.Parse(time.RFC3339, user.LastVisit)
	if err != nil {
		return err
	}

	if activity {
		acc := time.Duration(user.TotalVisit)
		user.TotalVisit = time.Since(lst.Add(-acc)).Nanoseconds()
	}

	user.LastVisit = time.Now().UTC().Format(time.RFC3339)

	err = s.bdb.SaveVisit(user.ID, user.LastVisit, user.TotalVisit)
	if err != nil {
		return err
	}

	return nil
}

// endVisit mark LastVisit to record timestamp of session end.
func (s *twitchService) endVisit(user *User) error {

	activity, err := s.hasElapsedFiveMins(user)
	if err != nil {
		return err
	}

	lst, err := time.Parse(time.RFC3339, user.LastVisit)
	if err != nil {
		return err
	}

	if activity {
		acc := time.Duration(user.TotalVisit)
		user.TotalVisit = time.Since(lst.Add(-acc)).Nanoseconds()
	}

	user.LastVisit = time.Now().UTC().Truncate(time.Hour).Format(time.RFC3339)

	err = s.bdb.SaveVisit(user.ID, user.LastVisit, user.TotalVisit)
	if err != nil {
		return err
	}

	return nil
}

// newMember returns a default member container.
func newMember(name string) (u *User) {

	utc := time.Now().UTC().Format(time.RFC3339)

	u = new(User)

	u.Username = name
	u.FirstVisit = utc
	u.LastVisit = utc
	u.TotalVisit = time.Nanosecond.Nanoseconds()
	u.Moderator = 0
	u.ReputationID = ""

	return u
}

//TODO trigger attached IRC scripts, transform note output before transmitting on wire
func (s *twitchService) cacheChat(v ChatJob) error {

	var err error
	tracer := stdopentracing.GlobalTracer()

	svc, err := restclient.New(s.httpRest, tracer, log.NewNopLogger())
	if err != nil {
		return err
	}

	err = svc.PostNote(context.Background(), newNote(v))
	if err != nil {
		return err
	}

	return nil
}

func newNote(j ChatJob) restsvc.Note {
	n := restsvc.Note{Model: "note", ID: j.ID}
	n.Attributes.Body = j.Attributes.Body
	n.Attributes.Who = j.Attributes.Who
	n.Attributes.Channel = j.Attributes.Channel
	n.Attributes.Host = j.Attributes.Host
	return n
}

// totalVisit read the total visit duration for the member.
func (s *twitchService) totalVisit(j ChatJob) (int64, error) {

	m, err := s.findMember(j.Attributes.Who)
	if err != nil {
		return 0, err
	}

	return m.TotalVisit, nil
}

var (
	// ErrUserNone is returned when a user is not found.
	ErrUserNone = errors.New("User Not Found, add user first.")
)
