package repeat

import (
	"fmt"
	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
//	"os"
//	"path"
	"strings"
	"time"

//	"github.com/patterns/re/record"
	"github.com/patterns/re/relay"
	grpcclient "github.com/patterns/re/relay/client/grpc"
)

type script struct {
	exec      int
	acl       int
	action    func(ChatJob) (int64, error)
	cooldown  time.Time
	soundfile string
}

// enumeration of script categories
const (
	StandardScript = 1
	SoundScript    = 2
)

// pre-defined script actions
const (
	TotalVisitHandler = 1
	BelayClipHandler  = 2
)

func (s *twitchService) MakeHandler(cmd string, cat int, action int) {

	//TODO chain/middleware
	switch action {

	case TotalVisitHandler:
		s.s[cmd] = script{exec: StandardScript, acl: 0, action: s.totalVisit}

	case BelayClipHandler:
		s.s[cmd] = script{
			exec:      SoundScript,
			acl:       0,
			action:    s.belayClip,
			soundfile: "belay.au",
		}
	}
}

// runCustomScripts executes scripts hooked to defined bang commands.
func (s *twitchService) runCustomScripts(j ChatJob) error {

	if strings.HasPrefix(j.Attributes.Body, "!") {

		// Potential bang cmd
		k := strings.ToLower(strings.Split(j.Attributes.Body, " ")[0])

		//TODO thread-safe access to map
		if act, ok := s.s[k]; ok {

			// Action has a defintion
			if matchACL(j, act) {
				act.cooldown = time.Now().UTC()
				s.s[k] = act
				return s.executeChain(j, act)
			}
		}
	}

	return nil
}

// matchACL enforce access level with supported commands.
func matchACL(j ChatJob, a script) bool {
	//TODO not all commands are allowed for everyone

	// Arbitrary ten minute cooldown for now
	d := time.Now().UTC().Sub(a.cooldown)
	if d < (time.Minute * 10) {
		return false
	}

	return true
}

//TODO does each handler come from middleware ??
func (s *twitchService) executeChain(j ChatJob, a script) error {

	switch a.exec {

	case StandardScript:
		d, err := a.action(j)
		if err != nil {
			return err
		}
		if d != 0 {
			return s.broadcastCustom(j, formatDuration(d, j.Attributes.Who))
		}

	case SoundScript:
		//TODO script-action too single purpose, overloading until refactor
		j := ChatJob{Model: a.soundfile}
		_, err := a.action(j)
		if err != nil {
			return err
		}
	}

	return nil
}

// broadcastCustom write the chat message via the grpc svc.
func (s *twitchService) broadcastCustom(j ChatJob, txt string) error {

	var (
		notify string
		svc    relay.Service
		err    error
	)

	//TODO let main pass-in the external services
	conn, err := grpc.Dial(s.grpcAddr, grpc.WithInsecure(), grpc.WithTimeout(time.Second))
	if err != nil {
		return err
	}
	defer conn.Close()

	tracer := stdopentracing.GlobalTracer()
	svc = grpcclient.New(conn, tracer, log.NewNopLogger())

	notify = formatCustomNotify(txt)

	if len(notify) != 0 {
		err := svc.Chat(context.Background(), j.Attributes.Channel, notify)
		if err != nil {
			return err
		}
	}

	return nil
}

func formatCustomNotify(txt string) string {

	return txt
}

// predefinedScripts initialize actions available for scripting.
func (s *twitchService) predefinedScripts() {
	/* todo unecessary with middleware ?
	k := string(TotalVisitHandler)
	s.s[k] = script { exec: StandardScript, acl: 0, action: s.totalVisit }

	k2 := string(BelayClipHandler)
	s.s[k2] = script { exec: StandardScript, acl: 0, action: s.belayClip }
	*/
}

func formatDuration(d int64, member string) string {

	msh := "\xF0\x9F\x8D\x84"
	oct := "\xF0\x9F\x90\x99"
	t := time.Duration(d)
	txt := fmt.Sprintf(" %s %s %s is %v into mutating", msh, member, oct, t)
	return txt
}

func (s *twitchService) belayClip(j ChatJob) (int64, error) {

	err := playSound(j.Model)
	if err != nil {
		return 0, err
	}

	return 0, nil
}

