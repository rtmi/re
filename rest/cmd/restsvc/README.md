# Restsvc

Restsvc exposes REST-like calls to a local maildir store.

## Running / Development

* `restsvc -http.addr=localhost:8080`

```
curl -d '{"id": "irc.freenode.net", "type": "config", "attributes": {"name": "Freenode Network", "version": "RFC 1459"}, "relationships": {"channels":{"links":{"related": "/api/configs/irc.freenode.net/channels/"}}}}' http://localhost:8080/api/configs/
```

```
curl -d '{"id": "irc.freenode.net.#bot-test", "type": "channel", "attributes": { "name": "#bot-test" }, "relationships": { "notes": { "links":{"related": "/api/channels/irc.freenode.net.%23bot-test/notes/"} } } }' http://localhost:8080/api/configs/irc.freenode.net/channels/
```

## Further Reading / Useful Links

* [irc](https://github.com/sorcix/irc)
* [maildir](https://github.com/luksen/maildir)
* [go-kit](https://github.com/go-kit/kit/examples)



