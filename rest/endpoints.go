package restsvc

import (
	"net/url"
	"strings"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

// Endpoints collects all of the endpoints that compose a config service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
//
// In a server, it's useful for functions that need to operate on a per-endpoint
// basis. For example, you might pass an Endpoints to a function that produces
// an http.Handler, with each method (endpoint) wired up to a specific path. (It
// is probably a mistake in design to invoke the Service methods on the
// Endpoints struct in a server.)
//
// In a client, it's useful to collect individually constructed endpoints into a
// single type that implements the Service interface. For example, you might
// construct individual endpoints using transport/http.NewClient, combine them
// into an Endpoints, and return it to the caller as a Service.
type Endpoints struct {
	PostConfigEndpoint    endpoint.Endpoint
	GetConfigEndpoint     endpoint.Endpoint
	PutConfigEndpoint     endpoint.Endpoint
	PatchConfigEndpoint   endpoint.Endpoint
	DeleteConfigEndpoint  endpoint.Endpoint
	GetChannelsEndpoint   endpoint.Endpoint
	GetChannelEndpoint    endpoint.Endpoint
	PostChannelEndpoint   endpoint.Endpoint
	DeleteChannelEndpoint endpoint.Endpoint
	GetNotesEndpoint      endpoint.Endpoint
	PostNoteEndpoint      endpoint.Endpoint
}

// MakeServerEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the provided service. Useful in a restsvc
// server.
func MakeServerEndpoints(s Service) Endpoints {
	return Endpoints{
		PostConfigEndpoint:    MakePostConfigEndpoint(s),
		GetConfigEndpoint:     MakeGetConfigEndpoint(s),
		PutConfigEndpoint:     MakePutConfigEndpoint(s),
		PatchConfigEndpoint:   MakePatchConfigEndpoint(s),
		DeleteConfigEndpoint:  MakeDeleteConfigEndpoint(s),
		GetChannelsEndpoint:   MakeGetChannelsEndpoint(s),
		GetChannelEndpoint:    MakeGetChannelEndpoint(s),
		PostChannelEndpoint:   MakePostChannelEndpoint(s),
		DeleteChannelEndpoint: MakeDeleteChannelEndpoint(s),
		GetNotesEndpoint:      MakeGetNotesEndpoint(s),
		PostNoteEndpoint:      MakePostNoteEndpoint(s),
	}
}

// MakeClientEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the remote instance, via a transport/http.Client.
// Useful in a restsvc client.
func MakeClientEndpoints(instance string) (Endpoints, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	tgt, err := url.Parse(instance)
	if err != nil {
		return Endpoints{}, err
	}
	tgt.Path = ""

	options := []httptransport.ClientOption{}

	// Note that the request encoders need to modify the request URL, changing
	// the path and method. That's fine: we simply need to provide specific
	// encoders for each endpoint.

	return Endpoints{
		PostConfigEndpoint:    httptransport.NewClient("POST", tgt, encodePostConfigRequest, decodePostConfigResponse, options...).Endpoint(),
		GetConfigEndpoint:     httptransport.NewClient("GET", tgt, encodeGetConfigRequest, decodeGetConfigResponse, options...).Endpoint(),
		PutConfigEndpoint:     httptransport.NewClient("PUT", tgt, encodePutConfigRequest, decodePutConfigResponse, options...).Endpoint(),
		PatchConfigEndpoint:   httptransport.NewClient("PATCH", tgt, encodePatchConfigRequest, decodePatchConfigResponse, options...).Endpoint(),
		DeleteConfigEndpoint:  httptransport.NewClient("DELETE", tgt, encodeDeleteConfigRequest, decodeDeleteConfigResponse, options...).Endpoint(),
		GetChannelsEndpoint:   httptransport.NewClient("GET", tgt, encodeGetChannelsRequest, decodeGetChannelsResponse, options...).Endpoint(),
		GetChannelEndpoint:    httptransport.NewClient("GET", tgt, encodeGetChannelRequest, decodeGetChannelResponse, options...).Endpoint(),
		PostChannelEndpoint:   httptransport.NewClient("POST", tgt, encodePostChannelRequest, decodePostChannelResponse, options...).Endpoint(),
		DeleteChannelEndpoint: httptransport.NewClient("DELETE", tgt, encodeDeleteChannelRequest, decodeDeleteChannelResponse, options...).Endpoint(),
		GetNotesEndpoint:      httptransport.NewClient("GET", tgt, encodeGetNotesRequest, decodeGetNotesResponse, options...).Endpoint(),
		PostNoteEndpoint:      httptransport.NewClient("POST", tgt, encodePostNoteRequest, decodePostNoteResponse, options...).Endpoint(),
	}, nil
}

// PostConfig implements Service. Primarily useful in a client.
func (e Endpoints) PostConfig(ctx context.Context, p Config) error {
	request := postConfigRequest{Config: p}
	response, err := e.PostConfigEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(postConfigResponse)
	return resp.Err
}

// GetConfig implements Service. Primarily useful in a client.
func (e Endpoints) GetConfig(ctx context.Context, id string) (Config, error) {
	request := getConfigRequest{ID: id}
	response, err := e.GetConfigEndpoint(ctx, request)
	if err != nil {
		return Config{}, err
	}
	resp := response.(getConfigResponse)
	return resp.Config, resp.Err
}

// PutConfig implements Service. Primarily useful in a client.
func (e Endpoints) PutConfig(ctx context.Context, id string, p Config) error {
	request := putConfigRequest{ID: id, Config: p}
	response, err := e.PutConfigEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(putConfigResponse)
	return resp.Err
}

// PatchConfig implements Service. Primarily useful in a client.
func (e Endpoints) PatchConfig(ctx context.Context, id string, p Config) error {
	request := patchConfigRequest{ID: id, Config: p}
	response, err := e.PatchConfigEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(patchConfigResponse)
	return resp.Err
}

// DeleteConfig implements Service. Primarily useful in a client.
func (e Endpoints) DeleteConfig(ctx context.Context, id string) error {
	request := deleteConfigRequest{ID: id}
	response, err := e.DeleteConfigEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(deleteConfigResponse)
	return resp.Err
}

// GetChannels implements Service. Primarily useful in a client.
func (e Endpoints) GetChannels(ctx context.Context, configID string) ([]Channel, error) {
	request := getChannelsRequest{ConfigID: configID}
	response, err := e.GetChannelsEndpoint(ctx, request)
	if err != nil {
		return nil, err
	}
	resp := response.(getChannelsResponse)
	return resp.Channels, resp.Err
}

// GetChannel implements Service. Primarily useful in a client.
func (e Endpoints) GetChannel(ctx context.Context, configID string, channelID string) (Channel, error) {
	request := getChannelRequest{ConfigID: configID, ChannelID: channelID}
	response, err := e.GetChannelEndpoint(ctx, request)
	if err != nil {
		return Channel{}, err
	}
	resp := response.(getChannelResponse)
	return resp.Channel, resp.Err
}

// PostChannel implements Service. Primarily useful in a client.
func (e Endpoints) PostChannel(ctx context.Context, configID string, a Channel) error {
	request := postChannelRequest{ConfigID: configID, Channel: a}
	response, err := e.PostChannelEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(postChannelResponse)
	return resp.Err
}

// DeleteChannel implements Service. Primarily useful in a client.
func (e Endpoints) DeleteChannel(ctx context.Context, configID string, channelID string) error {
	request := deleteChannelRequest{ConfigID: configID, ChannelID: channelID}
	response, err := e.DeleteChannelEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(deleteChannelResponse)
	return resp.Err
}

func (e Endpoints) GetNotes(ctx context.Context, id string) ([]Note, error) {
	request := getNotesRequest{ID: id}
	response, err := e.GetNotesEndpoint(ctx, request)
	if err != nil {
		return []Note{}, err
	}
	resp := response.(getNotesResponse)
	return resp.Notes, resp.Err
}

func (e Endpoints) PostNote(ctx context.Context, n Note) error {
	request := postNoteRequest{Model: n.Model, ID: n.ID}
	request.Attributes.Host = n.Attributes.Host
	request.Attributes.Channel = n.Attributes.Channel
	request.Attributes.Who = n.Attributes.Who
	request.Attributes.Body = n.Attributes.Body

	response, err := e.PostNoteEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(postNoteResponse)
	return resp.Err
}

// MakePostConfigEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakePostConfigEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(postConfigRequest)
		e := s.PostConfig(ctx, req.Config)
		return postConfigResponse{Err: e}, nil
	}
}

// MakeGetConfigEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetConfigEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getConfigRequest)
		p, e := s.GetConfig(ctx, req.ID)
		return getConfigResponse{Config: p, Err: e}, nil
	}
}

// MakePutConfigEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakePutConfigEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(putConfigRequest)
		e := s.PutConfig(ctx, req.ID, req.Config)
		return putConfigResponse{Err: e}, nil
	}
}

// MakePatchConfigEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakePatchConfigEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(patchConfigRequest)
		e := s.PatchConfig(ctx, req.ID, req.Config)
		return patchConfigResponse{Err: e}, nil
	}
}

// MakeDeleteConfigEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeDeleteConfigEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(deleteConfigRequest)
		e := s.DeleteConfig(ctx, req.ID)
		return deleteConfigResponse{Err: e}, nil
	}
}

// MakeGetChannelsEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetChannelsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getChannelsRequest)
		a, e := s.GetChannels(ctx, req.ConfigID)
		return getChannelsResponse{Channels: a, Err: e}, nil
	}
}

// MakeGetChannelEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetChannelEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getChannelRequest)
		a, e := s.GetChannel(ctx, req.ConfigID, req.ChannelID)
		return getChannelResponse{Channel: a, Err: e}, nil
	}
}

// MakePostChannelEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakePostChannelEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(postChannelRequest)
		e := s.PostChannel(ctx, req.ConfigID, req.Channel)
		return postChannelResponse{Err: e}, nil
	}
}

// MakeDeleteChannelEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeDeleteChannelEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(deleteChannelRequest)
		e := s.DeleteChannel(ctx, req.ConfigID, req.ChannelID)
		return deleteChannelResponse{Err: e}, nil
	}
}

func MakeGetNotesEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getNotesRequest)
		a, e := s.GetNotes(ctx, req.ID)
		return getNotesResponse{Notes: a, Err: e}, nil
	}
}

func MakePostNoteEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(postNoteRequest)
		n := Note{Model: req.Model, ID: req.ID}
		n.Attributes.Host = req.Attributes.Host
		n.Attributes.Channel = req.Attributes.Channel
		n.Attributes.Who = req.Attributes.Who
		n.Attributes.Body = req.Attributes.Body
		e := s.PostNote(ctx, n)
		return postNoteResponse{Err: e}, nil
	}
}

// We have two options to return errors from the business logic.
//
// We could return the error via the endpoint itself. That makes certain things
// a little bit easier, like providing non-200 HTTP responses to the client. But
// Go kit assumes that endpoint errors are (or may be treated as)
// transport-domain errors. For example, an endpoint error will count against a
// circuit breaker error count.
//
// Therefore, it's often better to return service (business logic) errors in the
// response object. This means we have to do a bit more work in the HTTP
// response encoder to detect e.g. a not-found error and provide a proper HTTP
// status code. That work is done with the errorer interface, in transport.go.
// Response types that may contain business-logic errors implement that
// interface.

type postConfigRequest struct {
	Config Config
}

type postConfigResponse struct {
	Err error `json:"err,omitempty"`
}

func (r postConfigResponse) error() error { return r.Err }

type getConfigRequest struct {
	ID string
}

type getConfigResponse struct {
	Config Config `json:"data,omitempty"`
	Err    error  `json:"err,omitempty"`
}

func (r getConfigResponse) error() error { return r.Err }

type putConfigRequest struct {
	ID     string
	Config Config
}

type putConfigResponse struct {
	Err error `json:"err,omitempty"`
}

func (r putConfigResponse) error() error { return nil }

type patchConfigRequest struct {
	ID     string
	Config Config
}

type patchConfigResponse struct {
	Err error `json:"err,omitempty"`
}

func (r patchConfigResponse) error() error { return r.Err }

type deleteConfigRequest struct {
	ID string
}

type deleteConfigResponse struct {
	Err error `json:"err,omitempty"`
}

func (r deleteConfigResponse) error() error { return r.Err }

type getChannelsRequest struct {
	ConfigID string
}

type getChannelsResponse struct {
	Channels []Channel `json:"data,omitempty"`
	Err      error     `json:"err,omitempty"`
}

func (r getChannelsResponse) error() error { return r.Err }

type getChannelRequest struct {
	ConfigID  string
	ChannelID string
}

type getChannelResponse struct {
	Channel Channel `json:"data,omitempty"`
	Err     error   `json:"err,omitempty"`
}

func (r getChannelResponse) error() error { return r.Err }

type postChannelRequest struct {
	ConfigID string
	Channel  Channel
}

type postChannelResponse struct {
	Err error `json:"err,omitempty"`
}

func (r postChannelResponse) error() error { return r.Err }

type deleteChannelRequest struct {
	ConfigID  string
	ChannelID string
}

type deleteChannelResponse struct {
	Err error `json:"err,omitempty"`
}

func (r deleteChannelResponse) error() error { return r.Err }

type getNotesRequest struct {
	ID string
}

type getNotesResponse struct {
	Notes []Note `json:"data,omitempty"`
	Err   error  `json:"err,omitempty"`
}

func (r getNotesResponse) error() error { return r.Err }

type postNoteRequest struct {
	Model      string `json:"type"`
	ID         string `json:"id"`
	Attributes struct {
		Who     string `json:"who,omitempty"`
		Body    string `json:"body,omitempty"`
		Channel string `json:"channel,omitempty"`
		Host    string `json:"host,omitempty"`
	} `json:"attributes"`
}

type postNoteResponse struct {
	Err error `json:"err,omitempty"`
}

func (r postNoteResponse) error() error { return r.Err }
