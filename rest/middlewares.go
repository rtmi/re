package restsvc

import (
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

// Middleware describes a service (as opposed to endpoint) middleware.
type Middleware func(Service) Service

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   Service
	logger log.Logger
}

func (mw loggingMiddleware) PostConfig(ctx context.Context, p Config) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PostConfig", "id", p.ID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PostConfig(ctx, p)
}

func (mw loggingMiddleware) GetConfig(ctx context.Context, id string) (p Config, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetConfig", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetConfig(ctx, id)
}

func (mw loggingMiddleware) PutConfig(ctx context.Context, id string, p Config) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PutConfig", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PutConfig(ctx, id, p)
}

func (mw loggingMiddleware) PatchConfig(ctx context.Context, id string, p Config) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PatchConfig", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PatchConfig(ctx, id, p)
}

func (mw loggingMiddleware) DeleteConfig(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "DeleteConfig", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.DeleteConfig(ctx, id)
}

func (mw loggingMiddleware) GetChannels(ctx context.Context, configID string) (channels []Channel, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetChannels", "configID", configID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetChannels(ctx, configID)
}

func (mw loggingMiddleware) GetChannel(ctx context.Context, configID string, channelID string) (a Channel, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetChannel", "configID", configID, "channelID", channelID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetChannel(ctx, configID, channelID)
}

func (mw loggingMiddleware) PostChannel(ctx context.Context, configID string, a Channel) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PostChannel", "configID", configID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PostChannel(ctx, configID, a)
}

func (mw loggingMiddleware) DeleteChannel(ctx context.Context, configID string, channelID string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "DeleteChannel", "configID", configID, "channelID", channelID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.DeleteChannel(ctx, configID, channelID)
}

func (mw loggingMiddleware) GetNotes(ctx context.Context, id string) (a []Note, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetNotes", "ID", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetNotes(ctx, id)
}

func (mw loggingMiddleware) PostNote(ctx context.Context, n Note) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PostNote", "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PostNote(ctx, n)
}
