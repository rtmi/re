package restsvc

import (
	"errors"
	"fmt"
	"net/url"
	"sync"

	"golang.org/x/net/context"
)

// Service is a simple CRUD interface for IRC configs.
type Service interface {
	PostConfig(ctx context.Context, p Config) error
	GetConfig(ctx context.Context, id string) (Config, error)
	PutConfig(ctx context.Context, id string, p Config) error
	PatchConfig(ctx context.Context, id string, p Config) error
	DeleteConfig(ctx context.Context, id string) error
	GetChannels(ctx context.Context, configID string) ([]Channel, error)
	GetChannel(ctx context.Context, configID string, channelID string) (Channel, error)
	PostChannel(ctx context.Context, configID string, a Channel) error
	DeleteChannel(ctx context.Context, configID string, channelID string) error
	GetNotes(ctx context.Context, id string) ([]Note, error)
	PostNote(ctx context.Context, n Note) error
}

// Config represents a IRC host-port config.
// ID should be globally unique.
type Config struct {
	Model         string       `json:"type"`
	ID            string       `json:"id"`
	Attributes    cfgattr      `json:"attributes"`
	Relationships cfgrelations `json:"relationships"`
}
type cfgattr struct {
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`
}
type cfgrelations struct {
	Channels  cfglinks `json:"channels,omitempty"`
	channames []string
}
type cfglinks struct {
	Links cfgrelated `json:"links,omitempty"`
}
type cfgrelated struct {
	Related string `json:"related"`
}

// Channel is a room on the IRC host.
// ID should be unique within the config (at a minimum).
type Channel struct {
	Model         string       `json:"type"`
	ID            string       `json:"id"`
	Attributes    chnattr      `json:"attributes"`
	Relationships chnrelations `json:"relationships"`
}
type chnattr struct {
	Name string `json:"name,omitempty"`
}
type chnrelations struct {
	Notes chnrelation `json:"notes,omitempty"`
}
type chnrelation struct {
	Links chnrelated `json:"links,omitempty"`
	keys  []string
}
type chnrelated struct {
	Related string `json:"related"`
}
type Note struct {
	Model      string   `json:"type"`
	ID         string   `json:"id"`
	Attributes noteattr `json:"attributes"`
}
type noteattr struct {
	Who     string `json:"who,omitempty"`
	Body    string `json:"body,omitempty"`
	headers map[string]string
	Channel string `json:"channel,omitempty"`
	Host    string `json:"host,omitempty"`
}

var (
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrAlreadyExists   = errors.New("already exists")
	ErrNotFound        = errors.New("not found")
)

type inmemService struct {
	mtx   sync.RWMutex
	m     map[string]Config
	h     map[string]Channel
	notes map[string]Note
}

func NewInmemService() Service {
	return &inmemService{
		m:     map[string]Config{},
		h:     map[string]Channel{},
		notes: map[string]Note{},
	}
}

func (s *inmemService) PostConfig(ctx context.Context, p Config) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	if _, ok := s.m[p.ID]; ok {
		return ErrAlreadyExists // POST = create, don't overwrite
	}
	s.m[p.ID] = p
	return nil
}

func (s *inmemService) GetConfig(ctx context.Context, id string) (Config, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	p, ok := s.m[id]
	if !ok {
		return Config{}, ErrNotFound
	}
	return p, nil
}

func (s *inmemService) PutConfig(ctx context.Context, id string, p Config) error {
	if id != p.ID {
		return ErrInconsistentIDs
	}
	s.mtx.Lock()
	defer s.mtx.Unlock()
	s.m[id] = p // PUT = create or update
	return nil
}

func (s *inmemService) PatchConfig(ctx context.Context, id string, p Config) error {
	if p.ID != "" && id != p.ID {
		return ErrInconsistentIDs
	}

	s.mtx.Lock()
	defer s.mtx.Unlock()

	existing, ok := s.m[id]
	if !ok {
		return ErrNotFound // PATCH = update existing, don't create
	}

	// We assume that it's not possible to PATCH the ID, and that it's not
	// possible to PATCH any field to its zero value. That is, the zero value
	// means not specified. The way around this is to use e.g. Name *string in
	// the Config definition. But since this is just a demonstrative example,
	// I'm leaving that out.

	if p.Attributes.Name != "" {
		existing.Attributes.Name = p.Attributes.Name
	}
	if len(p.Relationships.channames) > 0 {
		existing.Relationships.channames = p.Relationships.channames
	}
	s.m[id] = existing
	return nil
}

func (s *inmemService) DeleteConfig(ctx context.Context, id string) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	if p, ok := s.m[id]; ok {
		for _, v := range p.Relationships.channames {
			delete(s.h, v)
		}
		delete(s.m, id)
		return nil
	}
	return ErrNotFound
}

func (s *inmemService) GetChannels(ctx context.Context, configID string) ([]Channel, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	ls := []Channel{}
	p, ok := s.m[configID]
	if !ok {
		return ls, ErrNotFound
	}
	for _, v := range p.Relationships.channames {
		if ch, ok := s.h[v]; ok {
			ls = append(ls, ch)
		}
	}
	return ls, nil
}

func (s *inmemService) GetChannel(ctx context.Context, configID string, channelID string) (Channel, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	ch, ok := s.h[channelID]
	if !ok {
		return Channel{}, ErrNotFound
	}
	return ch, nil
}

func (s *inmemService) PostChannel(ctx context.Context, configID string, a Channel) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	p, ok := s.m[configID]
	if !ok {
		return ErrNotFound
	}
	if _, ok := s.h[a.ID]; ok {
		return ErrAlreadyExists
	}
	p.Relationships.channames = append(p.Relationships.channames, a.ID)
	s.m[configID] = p
	s.h[a.ID] = a
	return nil
}

func (s *inmemService) DeleteChannel(ctx context.Context, configID string, channelID string) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	p, ok := s.m[configID]
	if !ok {
		return ErrNotFound
	}
	newChannels := make([]string, 0, len(p.Relationships.channames))
	for _, name := range p.Relationships.channames {
		if name == channelID {
			continue // delete
		}
		newChannels = append(newChannels, channelID)
	}
	if len(newChannels) == len(p.Relationships.channames) {
		return ErrNotFound
	}
	p.Relationships.channames = newChannels
	s.m[configID] = p
	delete(s.h, channelID)
	return nil
}

func (s *inmemService) GetNotes(ctx context.Context, id string) ([]Note, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	_, ok := s.h[id]
	if !ok {
		return []Note{}, ErrNotFound
	}

	a := s.retrieveNotes(id)
	return a, nil
}

func (s *inmemService) PostNote(ctx context.Context, n Note) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	if _, ok := s.notes[n.ID]; ok {
		return ErrAlreadyExists
	}

	s.notes[n.ID] = n

	var err error
	cid := fmt.Sprintf("%v.%v", n.Attributes.Host, n.Attributes.Channel)
	ch, ok := s.h[cid]
	if !ok {
		ch, err = s.addChannel(n, cid)
		if err != nil {
			return err
		}
	}

	ch.Relationships.Notes.keys = append(ch.Relationships.Notes.keys, n.ID)
	s.h[cid] = ch
	return nil
}

func (s *inmemService) addChannel(n Note, cid string) (Channel, error) {
	// If the channel doesn't exist yet, add it as newly created.
	// (Example, the pseudo channel "Verbose")
	conf, ok := s.m[n.Attributes.Host]
	if !ok {
		return Channel{}, ErrNotFound
	}
	a := chnattr{Name: n.Attributes.Channel}
	u := chnrelated{Related: fmt.Sprintf("/api/channels/%v/notes/", url.QueryEscape(cid))}
	l := chnrelation{Links: u, keys: []string{}}
	r := chnrelations{Notes: l}
	ch := Channel{Model: "channel", ID: cid, Attributes: a, Relationships: r}
	s.h[cid] = ch
	conf.Relationships.channames = append(conf.Relationships.channames, cid)
	s.m[n.Attributes.Host] = conf
	return ch, nil
}

func (s *inmemService) retrieveNotes(channelID string) []Note {
	ch, _ := s.h[channelID]
	sz := len(ch.Relationships.Notes.keys)
	a := make([]Note, 0, sz)
	for _, v := range ch.Relationships.Notes.keys {
		a = append(a, s.notes[v])
	}
	return a
}
