package restsvc

// The restsvc is just over HTTP, so we just have a single transport.go.

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
)

var (
	// ErrBadRouting is returned when an expected path variable is missing.
	// It always indicates programmer error.
	ErrBadRouting = errors.New("inconsistent mapping between route and handler (programmer error)")
)

// MakeHTTPHandler mounts all of the service endpoints into an http.Handler.
// Useful in a restsvc server.
func MakeHTTPHandler(ctx context.Context, s Service, logger log.Logger) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(s)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(encodeError),
	}

	// POST    /configs/                          adds another config
	// GET     /configs/:id                       retrieves the given config by id
	// PUT     /configs/:id                       post updated config information about the config
	// PATCH   /configs/:id                       partial updated config information
	// DELETE  /configs/:id                       remove the given config
	// GET     /configs/:id/channels/            retrieve channels associated with the config
	// GET     /configs/:id/channels/:channelID  retrieve a particular config channel
	// POST    /configs/:id/channels/            add a new channel
	// DELETE  /configs/:id/channels/:channelID  remove an channel

	r.Methods("POST").Path("/api/configs/").Handler(httptransport.NewServer(
		ctx,
		e.PostConfigEndpoint,
		decodePostConfigRequest,
		encodeResponse,
		options...,
	))
	r.Methods("GET").Path("/api/configs/{id}").Handler(httptransport.NewServer(
		ctx,
		e.GetConfigEndpoint,
		decodeGetConfigRequest,
		encodeResponse,
		options...,
	))
	r.Methods("PUT").Path("/api/configs/{id}").Handler(httptransport.NewServer(
		ctx,
		e.PutConfigEndpoint,
		decodePutConfigRequest,
		encodeResponse,
		options...,
	))
	r.Methods("PATCH").Path("/api/configs/{id}").Handler(httptransport.NewServer(
		ctx,
		e.PatchConfigEndpoint,
		decodePatchConfigRequest,
		encodeResponse,
		options...,
	))
	r.Methods("DELETE").Path("/api/configs/{id}").Handler(httptransport.NewServer(
		ctx,
		e.DeleteConfigEndpoint,
		decodeDeleteConfigRequest,
		encodeResponse,
		options...,
	))
	r.Methods("GET").Path("/api/configs/{id}/channels/").Handler(httptransport.NewServer(
		ctx,
		e.GetChannelsEndpoint,
		decodeGetChannelsRequest,
		encodeResponse,
		options...,
	))
	r.Methods("GET").Path("/api/configs/{id}/channels/{channelID}").Handler(httptransport.NewServer(
		ctx,
		e.GetChannelEndpoint,
		decodeGetChannelRequest,
		encodeResponse,
		options...,
	))
	r.Methods("POST").Path("/api/configs/{id}/channels/").Handler(httptransport.NewServer(
		ctx,
		e.PostChannelEndpoint,
		decodePostChannelRequest,
		encodeResponse,
		options...,
	))
	r.Methods("DELETE").Path("/api/configs/{id}/channels/{channelID}").Handler(httptransport.NewServer(
		ctx,
		e.DeleteChannelEndpoint,
		decodeDeleteChannelRequest,
		encodeResponse,
		options...,
	))

	// Convenience routes to help consumers nesting limitations
	r.Methods("GET").Path("/api/channels/{channelID}/notes/").Handler(httptransport.NewServer(
		ctx,
		e.GetNotesEndpoint,
		decodeGetNotesRequest,
		encodeResponse,
		options...,
	))
	r.Methods("GET").Path("/api/channels/{channelID}").Handler(httptransport.NewServer(
		ctx,
		e.GetChannelEndpoint,
		decodeGetChannelRequest,
		encodeResponse,
		options...,
	))
	r.Methods("POST").Path("/api/notes/").Handler(httptransport.NewServer(
		ctx,
		e.PostNoteEndpoint,
		decodePostNoteRequest,
		encodeResponse,
		options...,
	))

	return r
}

func decodePostConfigRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req postConfigRequest
	if e := json.NewDecoder(r.Body).Decode(&req.Config); e != nil {
		return nil, e
	}
	return req, nil
}

func decodeGetConfigRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	return getConfigRequest{ID: id}, nil
}

func decodePutConfigRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	var config Config
	if err := json.NewDecoder(r.Body).Decode(&config); err != nil {
		return nil, err
	}
	return putConfigRequest{
		ID:     id,
		Config: config,
	}, nil
}

func decodePatchConfigRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	var config Config
	if err := json.NewDecoder(r.Body).Decode(&config); err != nil {
		return nil, err
	}
	return patchConfigRequest{
		ID:     id,
		Config: config,
	}, nil
}

func decodeDeleteConfigRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	return deleteConfigRequest{ID: id}, nil
}

func decodeGetChannelsRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	return getChannelsRequest{ConfigID: id}, nil
}

func decodeGetChannelRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	channelID, ok := vars["channelID"]
	if !ok {
		return nil, ErrBadRouting
	}
	i, err := url.QueryUnescape(channelID)
	if err != nil {
		return nil, ErrBadRouting
	}
	return getChannelRequest{
		ChannelID: i,
	}, nil
}

func decodePostChannelRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	var channel Channel
	if err := json.NewDecoder(r.Body).Decode(&channel); err != nil {
		return nil, err
	}

	return postChannelRequest{
		ConfigID: id,
		Channel:  channel,
	}, nil
}

func decodeDeleteChannelRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, ErrBadRouting
	}
	channelID, ok := vars["channelID"]
	if !ok {
		return nil, ErrBadRouting
	}
	return deleteChannelRequest{
		ConfigID:  id,
		ChannelID: channelID,
	}, nil
}

func decodeGetNotesRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	channelID, ok := vars["channelID"]
	if !ok {
		return nil, ErrBadRouting
	}
	i, err := url.QueryUnescape(channelID)
	if err != nil {
		return nil, ErrBadRouting
	}
	return getNotesRequest{
		ID: i,
	}, nil
}

func decodePostNoteRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		return nil, err
	}

	n := postNoteRequest{
		Model: note.Model,
		ID:    note.ID,
	}
	n.Attributes.Host = note.Attributes.Host
	n.Attributes.Channel = note.Attributes.Channel
	n.Attributes.Who = note.Attributes.Who
	n.Attributes.Body = note.Attributes.Body
	return n, nil
}

func encodePostConfigRequest(ctx context.Context, req *http.Request, request interface{}) error {
	req.Method, req.URL.Path = "POST", "/api/configs/"
	return encodeRequest(ctx, req, request)
}

func encodeGetConfigRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(getConfigRequest)
	configID := url.QueryEscape(r.ID)
	req.Method, req.URL.Path = "GET", "/api/configs/"+configID
	return encodeRequest(ctx, req, request)
}

func encodePutConfigRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(putConfigRequest)
	configID := url.QueryEscape(r.ID)
	req.Method, req.URL.Path = "PUT", "/api/configs/"+configID
	return encodeRequest(ctx, req, request)
}

func encodePatchConfigRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(patchConfigRequest)
	configID := url.QueryEscape(r.ID)
	req.Method, req.URL.Path = "PATCH", "/api/configs/"+configID
	return encodeRequest(ctx, req, request)
}

func encodeDeleteConfigRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(deleteConfigRequest)
	configID := url.QueryEscape(r.ID)
	req.Method, req.URL.Path = "DELETE", "/api/configs/"+configID
	return encodeRequest(ctx, req, request)
}

func encodeGetChannelsRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(getChannelsRequest)
	configID := url.QueryEscape(r.ConfigID)
	req.Method, req.URL.Path = "GET", "/api/configs/"+configID+"/channels/"
	return encodeRequest(ctx, req, request)
}

func encodeGetChannelRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(getChannelRequest)
	channelID := url.QueryEscape(r.ChannelID)
	req.Method, req.URL.Path = "GET", "/api/channels/"+channelID
	return encodeRequest(ctx, req, request)
}

func encodePostChannelRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(postChannelRequest)
	configID := url.QueryEscape(r.ConfigID)
	req.Method, req.URL.Path = "POST", "/api/configs/"+configID+"/channels/"
	return encodeRequest(ctx, req, request)
}

func encodeDeleteChannelRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(deleteChannelRequest)
	configID := url.QueryEscape(r.ConfigID)
	channelID := url.QueryEscape(r.ChannelID)
	req.Method, req.URL.Path = "DELETE", "/api/configs/"+configID+"/channels/"+channelID
	return encodeRequest(ctx, req, request)
}

func encodeGetNotesRequest(ctx context.Context, req *http.Request, request interface{}) error {
	r := request.(getNotesRequest)
	id := url.QueryEscape(r.ID)
	req.Method, req.URL.Path = "GET", "/api/channels/"+id+"/notes/"
	return encodeRequest(ctx, req, request)
}

func encodePostNoteRequest(ctx context.Context, req *http.Request, request interface{}) error {
	//	r := request.(postNoteRequest)
	req.Method, req.URL.Path = "POST", "/api/notes/"
	return encodeRequest(ctx, req, request)
}

func decodePostConfigResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response postConfigResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeGetConfigResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response getConfigResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodePutConfigResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response putConfigResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodePatchConfigResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response patchConfigResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeDeleteConfigResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response deleteConfigResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeGetChannelsResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response getChannelsResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeGetChannelResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response getChannelResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodePostChannelResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response postChannelResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeDeleteChannelResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response deleteChannelResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodeGetNotesResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response getNotesResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

func decodePostNoteResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response postNoteResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

// errorer is implemented by all concrete response types that may contain
// errors. It allows us to change the HTTP response code without needing to
// trigger an endpoint (transport-level) error. For more information, read the
// big comment in endpoints.go.
type errorer interface {
	error() error
}

// encodeResponse is the common method to encode all response types to the
// client. I chose to do it this way because, since we're using JSON, there's no
// reason to provide anything more specific. It's certainly possible to
// specialize on a per-response (per-method) basis.
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// encodeRequest likewise JSON-encodes the request to the HTTP request body.
// Don't use it directly as a transport/http.Client EncodeRequestFunc:
// restsvc endpoints require mutating the HTTP method and request path.
func encodeRequest(_ context.Context, req *http.Request, request interface{}) error {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(request)
	if err != nil {
		return err
	}
	req.Body = ioutil.NopCloser(&buf)
	return nil
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case ErrNotFound:
		return http.StatusNotFound
	case ErrAlreadyExists, ErrInconsistentIDs:
		return http.StatusBadRequest
	default:
		if e, ok := err.(httptransport.Error); ok {
			switch e.Domain {
			case httptransport.DomainDecode:
				return http.StatusBadRequest
			case httptransport.DomainDo:
				return http.StatusServiceUnavailable
			default:
				return http.StatusInternalServerError
			}
		}
		return http.StatusInternalServerError
	}
}
