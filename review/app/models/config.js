import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  version: DS.attr('string'),
  channels: DS.hasMany('channel', { async: true }),

});
