import DS from 'ember-data';

export default DS.Model.extend({
    who: DS.attr('string'),
    body: DS.attr('string'),
    channel: DS.belongsTo('channel')
});
