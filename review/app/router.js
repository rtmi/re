import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('configs', { path: '/configs/:config_id'});
  this.route('channels', { path: '/channels/:channel_id'});
});

export default Router;
