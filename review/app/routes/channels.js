import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('channel', params.channel_id, {
           reload: true,
           include: 'notes'
    });
  },
  setupController(controller, model) {
        this._super(...arguments);
        this.get('pollServerForChanges').perform(model);
  },
  pollServerForChanges: task(function * (model) {
        yield timeout(500);
        while (true) {
            yield timeout(5000);
            model.hasMany('notes').reload();
        }
  }).cancelOn('deactivate').restartable(),
});
