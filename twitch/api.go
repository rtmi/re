package twitch

import (
	"encoding/json"
	"net/http"
	"time"
)

func Get(clientID string, api string) (*json.Decoder, error) {

	var err error
	w := &http.Client{
		Timeout: time.Second * 120,
	}

	req, err := http.NewRequest("GET", api, nil)
	if err != nil {
		return &json.Decoder{}, err
	}

	req.Header.Add("Accept", "application/vnd.twitchtv.v5+json")
	req.Header.Add("Client-ID", clientID)

	res, err := w.Do(req)
	if err != nil {
		return &json.Decoder{}, err
	}

	return json.NewDecoder(res.Body), nil
}
