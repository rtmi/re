package twitch

import (
	"time"
)

type FollowsResponse struct {
	Total   int    `json:"_total"`
	Cursor  string `json:"_cursor"`
	Follows []struct {
		CreatedAt     time.Time `json:"created_at"`
		Notifications bool      `json:"notifications"`
		User          struct {
			DisplayName string    `json:"display_name"`
			ID          string    `json:"_id"`
			Name        string    `json:"name"`
			Type        string    `json:"type"`
			Bio         string    `json:"bio"`
			CreatedAt   time.Time `json:"created_at"`
			UpdatedAt   time.Time `json:"updated_at"`
			Logo        string    `json:"logo"`
		} `json:"user"`
	} `json:"follows"`
}

/* v3 ??
type FollowsResponse struct {
	Total int `json:"_total"`
	Links struct {
		Self string `json:"self"`
		Next string `json:"next"`
	} `json:"_links"`
	Cursor  string `json:"_cursor"`
	Follows []struct {
		CreatedAt time.Time `json:"created_at"`
		Links     struct {
			Self string `json:"self"`
		} `json:"_links"`
		Notifications bool `json:"notifications"`
		User          struct {
			DisplayName string      `json:"display_name"`
			ID          int         `json:"_id"`
			Name        string      `json:"name"`
			Type        string      `json:"type"`
			Bio         interface{} `json:"bio"`
			CreatedAt   time.Time   `json:"created_at"`
			UpdatedAt   time.Time   `json:"updated_at"`
			Logo        interface{} `json:"logo"`
			Links       struct {
				Self string `json:"self"`
			} `json:"_links"`
		} `json:"user"`
	} `json:"follows"`
}
*/
