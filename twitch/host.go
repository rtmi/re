package twitch

type HostsResponse struct {
	Hosts []struct {
		HostID        int  `json:"host_id"`
		TargetID      int  `json:"target_id"`
		HostPartnered bool `json:"host_partnered,omitempty"`
	} `json:"hosts"`
}
